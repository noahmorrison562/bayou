import java.util.ArrayList;


public class Client extends Process 
{
	volatile ArrayList<ChatLogLine> chat = new ArrayList<ChatLogLine>();
	public Client(ProcessId me,int p,int totalNodes)
	{
		super(p);
		this.me = me;
		this.totalNodes = totalNodes;
	}
	
	public void body()
	{
		//run forever maybe listening for stuff from controller?
		while(true)
		{
			executeInstruction();
			recieveAll();			
			PaxosMessage msg = getMessage();
			if(msg instanceof RequestMessage)
			{
				//System.out.println("mistake!");
			}
			if(msg instanceof DecisionMessage)
			{
				
				DecisionMessage d = (DecisionMessage) msg;
				//System.out.println(this.me+" recieved decision value from "+d.src);
				int i = d.slot_number;
				Command c = d.command;
				ChatLogLine cll = new ChatLogLine(i,c.client.getIndex()-totalNodes,c.chatMessage);
				while(chat.size()<=i)
				{
					chat.add(null);
				}
				if(chat.get(i)==null)
				{
					chat.set(i,cll);
				}
				//System.out.println(chat.get(0));
				removeMessage();
				//System.out.println("client done");
			}
		}
	}
	
}
