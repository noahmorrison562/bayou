import java.util.HashSet;
import java.util.Set;


public class Scout extends Process 
{
	ProcessId leader;
	ProcessId[] acceptors;
	BallotNumber ballot_number;
	Server lead;
	boolean active = false;
	public Scout(Server s,int p,ProcessId me, ProcessId leader, ProcessId[] acceptors, BallotNumber ballot_number)
	{
		super(p);
		//System.out.println("New Scout!");
		this.me = me;
		this.acceptors = acceptors;
		this.leader = leader;
		this.ballot_number = ballot_number;
		this.lead = s;
		
		//set up connections so scout can send and recieve messages
		//
	}
	public void body()
	{
		////System.out.println(this.me);
		P1aMessageScout m1 = new P1aMessageScout(me,ballot_number,true);
		Set<Integer> waitFor = new HashSet<Integer>();
		//for all a in acceptors, send m1 to a, add a to waitfor set
		send(leader.getIndex(),m1);
		////System.out.println("sent to leader from scout");
		Set<Pvalue> pvalues = new HashSet<Pvalue>();
		for(ProcessId a:acceptors)
		{
			waitFor.add(a.getIndex());
		}
		while(2*waitFor.size() >=acceptors.length)
		{
			recieveAll();
			////System.out.println("scout recieved");
			//receive message and store in
			PaxosMessage msg = getMessage();
			if(msg instanceof P1bMessage)
			{
				P1bMessage m = (P1bMessage)msg;
				//System.out.println(m.src);
				//System.out.println(this.me+"scout recieved P1bMessage from "+m.src);
				////System.out.println(ballot_number);
				int cmp = ballot_number.compareTo(m.ballot_number);
				if(cmp!=0)
				{
					//send a preempted message to the leader
					////System.out.println("sent leader preempted message");
					PreemptedMessage preMessage = new PreemptedMessage(this.me,ballot_number);
					send(leader.getIndex(),preMessage);
					//System.out.println("scout done");
					return;
				}
				if(waitFor.contains(m.src.getIndex()))
				{
					waitFor.remove(m.src.getIndex());
					//System.out.println("removed from waitFor set: "+m.src);
					pvalues.addAll(m.accepted);
				}
				removeMessage();
			}
		}
		//send an adopted message to the leader
		//System.out.println("send adopted message to the leader");
		AdoptedMessage adoptMessage = new AdoptedMessage(this.me,ballot_number,pvalues);
		send(leader.getIndex(),adoptMessage);
		//System.out.println("scout done");
	}
	public void run()
	{
		while(!this.active)
		{
			executeInstruction();
		}
		
		body();
		
	}
	
}
