
public class ChatLogLine 
{
	int sequenceNumber;
	//make so for creating clients their process id is just a number?
	int senderIndex;
	String message;
	
	public ChatLogLine(int sn, int client, String m)
	{
		sequenceNumber = sn;
		senderIndex = client;
		message = m;
	}
	public String toString()
	{
		if(!message.equals("NOP"))
		{
			return ""+sequenceNumber+" "+senderIndex+": "+message;
		}
		else
		{
			return "";
		}
	}
}
