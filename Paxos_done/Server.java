import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;


public class Server extends Process 
{
	boolean acceptor;
	boolean replica;
	ProcessId ProcessId;
	int quorum;
	int scoutNum;
	int slot_num=0;
	Map<Integer, Command> proposalsReplica = new HashMap<Integer,Command>();
	Map<Integer, Command> proposalsLeader = new HashMap<Integer, Command>();
	volatile Map<Integer, Command> decisionsReplica = new HashMap<Integer, Command>();
	BallotNumber ballot_numberAcceptor = null;
	Set<Pvalue> acceptedAcceptor = new HashSet<Pvalue>();
	volatile ProcessId[] acceptors;
	volatile ProcessId[] replicas;
	boolean active = false;
	volatile int timeBomb=-1;
	ReentrantLock timeBombLock;
	BallotNumber ballot_numberLeader = new BallotNumber(0,this.me);
	ArrayList<Scout> scouts = new ArrayList<Scout>();
	ArrayList<Commander> commanders = new ArrayList<Commander>();

	
	Server(ReentrantLock tbl,int p, boolean leader, boolean acceptor,boolean replica,int q,ProcessId l,ProcessId m,int i,int sNum)
	{
		super(p);
		this.isLeader = leader;
		this.acceptor = acceptor;
		this.replica = replica;
		this.scoutNum = sNum;
		this.numProcesses = sNum;
		this.quorum=q;
		this.totalNodes = i;
		ProcessId[] a = new ProcessId[this.totalNodes];
		ProcessId[] r = new ProcessId[this.totalNodes];
		for(int f=0;f<this.totalNodes;f++)
		{
			a[f] = new ProcessId(f,"server");
			r[f] = new ProcessId(f,"server");
		}
		this.acceptors = a;
		this.replicas = r;
		this.leader = l;
		this.me = m;
		this.timeBombLock = tbl;
	}
	public void leader()
	{
			//get next message
			PaxosMessage msg = getMessage();
			if(msg instanceof ProposeMessage)
			{
				ProposeMessage m = (ProposeMessage) msg;
				////System.out.println("leader "+this.me+" received propose message from "+m.src);
				if(!proposalsLeader.containsKey(m.slot_number))
				{
					proposalsLeader.put(m.slot_number, m.command);
					if(active)
					{
						////System.out.println("trying to make commander?");
						Commander c = new Commander(this,scoutNum,new ProcessId(scoutNum,"commander"),me,acceptors,replicas,ballot_numberLeader,m.slot_number,m.command);
						c.start();
						createConnections(c.p,c);
						c.active = true;
						scoutNum++;
						commanders.add(c);
					}
				}
				removeMessage();
			}
			else if(msg instanceof AdoptedMessage)
			{
				AdoptedMessage m = (AdoptedMessage) msg;
				////System.out.println("leader "+this.me+" recieved adopted message from "+m.src);
				if(ballot_numberLeader.equals(m.ballot_number))
				{
					////System.out.println("equal ballot numbers");
					Map<Integer, BallotNumber> max = new HashMap<Integer, BallotNumber>();
					for(Pvalue pv: m.accepted)
					{
						BallotNumber bn = max.get(pv.slot_number);
						if(bn==null || bn.compareTo(pv.ballot_number)<0)
						{
							max.put(pv.slot_number,pv.ballot_number);
							proposalsLeader.put(pv.slot_number, pv.command);
						}
					}
					for(int sn: proposalsLeader.keySet())
					{
						Commander c = new Commander(this,scoutNum,new ProcessId(scoutNum,"commander"),me,acceptors, replicas,ballot_numberLeader,sn,proposalsLeader.get(sn));
						c.start();
						createConnections(c.p,c);
						////System.out.println("finished create connections");
						c.active = true;
						scoutNum++;
						commanders.add(c);

					}
					this.active=true;
				}
				removeMessage();
				////System.out.println("finished adopted");
			}
			else if(msg instanceof PreemptedMessage)
			{
				
				PreemptedMessage m = (PreemptedMessage) msg;
				////System.out.println("leader "+this.me+" recieved preempted message from "+m.src);
				if(ballot_numberLeader.compareTo(m.ballot_number)<0)
				{
					ballot_numberLeader = new BallotNumber(m.ballot_number.round+1,me);
					
					Scout s = new Scout(this,scoutNum,new ProcessId(scoutNum,"scout"),me,acceptors,ballot_numberLeader);
					s.start();
					createConnections(s.p,s);
					s.active = true;
					scoutNum++;
					scouts.add(s);
					active = false;
				}
				removeMessage();
			}
			else if(msg instanceof P2bMessage)
			{
				
				//P2bMessage to send to the commander
				P2bMessage m = (P2bMessage)msg;
				////System.out.println("leader "+this.me+" recieved P2bMessage from "+m.src);
				send(m.commander,m);
				removeMessage();
			}
			else if(msg instanceof P1bMessage)
			{
				
				//message to send to the scout
				P1bMessage m = (P1bMessage)msg;
				////System.out.println("leader "+this.me+" recieved P1bMessage from "+m.src);
				////System.out.println(m.scout);
				P1bMessage m1 = new P1bMessage(m.src,m.ballot_number,m.accepted,m.scout);
				send(m.scout,m1);
				////System.out.println(m.scout);
				removeMessage();
				////System.out.println("passed message to scout and removed from queue");
			}
			else if(msg instanceof CommandDecisionMessage)
			{
				
				CommandDecisionMessage cdm = (CommandDecisionMessage)msg;
				////System.out.println("leader "+this.me+" recieved CommandDecisionMessage from "+cdm.src);
				DecisionMessage dm = new DecisionMessage(cdm.src,cdm.slot_number,cdm.command);
				sendAllServers(dm);
				removeMessage();
			}
			else if(msg instanceof P1aMessageScout && this.isLeader)
			{
				
				//message from the scout to send to all acceptors
				P1aMessageScout m = (P1aMessageScout)msg;
				////System.out.println("Leader "+this.me+" recieved P1aMessageScout from "+m.src);
				P1aMessage m1 = new P1aMessage(m.src,m.ballot_number);
				for(int i=0;i<totalNodes;i++)
				{
					ProcessId a = acceptors[i];
					timeBombLock.lock();
					try
					{
						////System.out.println("got lock");
						if(timeBomb!=0)
						{
							send(a.getIndex(),m1);
							////System.out.println("sent P1aMessage to "+a);
							timeBomb--;
						}
						else
						{
							//set instruction to die and execute instruction
							timeBombLock.unlock();
							die();
							break;
						}
					}
					finally
					{
						timeBombLock.unlock();
					}
				}
				removeMessage();
			}
			else if(msg instanceof P2aMessageCommander && this.isLeader)
			{
				
				//message from the commander to send to all the acceptors
				//cant use sendAllServers due to potential timeBombLeader
				//sendAllServers(msg);
				P2aMessageCommander m = (P2aMessageCommander)msg;
				////System.out.println("leader "+this.me+" recieved P2aMessageCommander from "+m.src);
				P2aMessage m1 = new P2aMessage(m.src,m.ballot_number,m.slot_number,m.command);
				for(ProcessId a: acceptors)
				{
					timeBombLock.lock();
					try
					{
						if(timeBomb!=0)
						{
							send(a.getIndex(),m1);
							////System.out.println("sent P2aMessage to "+a);
							timeBomb--;
						}
						else
						{
							timeBombLock.unlock();
							die();
							break;
						}
					}
					finally
					{
						timeBombLock.unlock();
					}
					
				}
				removeMessage();
			}
		
	}
	public void acceptor()
	{
		
		//add me as acceptor connector to send and recieve messages
		//receive next message
		PaxosMessage msg = getMessage();
		if(msg instanceof P1aMessage)
		{
			
			P1aMessage m = (P1aMessage) msg;
			////System.out.println("acceptor "+this.me+" recieved P1aMessage from "+m.src);
			if(ballot_numberAcceptor ==null || ballot_numberAcceptor.compareTo(m.ballot_number)<0)
			{
				ballot_numberAcceptor = m.ballot_number;
			}
			//send P1b message to m.src
			P1bMessage p1bMessage = new P1bMessage(this.me,ballot_numberAcceptor,acceptedAcceptor,m.src.getIndex());
			send(this.leader.getIndex(),p1bMessage);
			removeMessage();
		}
		else if(msg instanceof P2aMessage)
		{
			P2aMessage m = (P2aMessage) msg;
			////System.out.println("acceptor "+this.me+" recieved P2aMessage from "+m.src);
			if(ballot_numberAcceptor==null || ballot_numberAcceptor.compareTo(m.ballot_number)<=0)
			{
				ballot_numberAcceptor = m.ballot_number;
				acceptedAcceptor.add(new Pvalue(ballot_numberAcceptor, m.slot_number,m.command));
			}
			//send P2bMessage to m.src
			P2bMessage p2bMessage = new P2bMessage(this.me,ballot_numberAcceptor,m.slot_number,m.src.getIndex());
			send(this.leader.getIndex(),p2bMessage);
			removeMessage();
		}
	}
	public void replica()
	{
		
			//receive next message
			PaxosMessage msg = getMessage();

			if(msg instanceof AliveMessage)
			{
				StateMessage sm = new StateMessage(this.me,decisionsReplica,leader);
				AliveMessage am = (AliveMessage)msg;
				////System.out.println("ALIVE MESSAGE from: "+am.src+" to: "+this.me);
				send(am.src.getIndex(),sm);
				removeMessage();
			}
			else if(msg instanceof StateMessage)
			{
				StateMessage sm = (StateMessage)msg;
				if(this.leader == null)
				{
					this.leader = sm.leader;
				}
				////System.out.println("STATE MESSAGE from: "+sm.src+" to: "+this.me);
				this.decisionsReplica.putAll(sm.decisions);
				removeMessage();
			}
			else if(msg instanceof RequestMessage)
			{
				
				RequestMessage m = (RequestMessage) msg;
				////System.out.println("replica "+this.me+" recieved request message from "+m.src);
				Command c = m.command;
				//propose method start
				propose(c,leader.getIndex());
				//propose method end
				removeMessage();
			}
			else if(msg instanceof DecisionMessage)
			{
				
				DecisionMessage m = (DecisionMessage) msg;
				////System.out.println("replica "+this.me+" recieved decision message from "+m.src);
				decisionsReplica.put(m.slot_number,  m.command);
				////System.out.println(m.command);
				for(;;)
				{
					Command c = decisionsReplica.get(slot_num);
					if(c==null)
					{
						////System.out.println("null command");
						break;
					}
					Command c2 = proposalsReplica.get(slot_num);
					if(c2!=null && !c2.equals(c))
					{
						//propose start
						////System.out.println("propose");
						propose(c2,leader.getIndex());
						//propose end
					}
					// perform c, have to check what this entails
					////System.out.println("perform");
					perform(c);
				}
				removeMessage();
			}
	}
	public void body()
	{
		while(true)
		{
			executeInstruction();
			if(isLeader)
			{
				sendAllServers(new HeartbeatMessage(this.me));
			}
			recieveAll();
			PaxosMessage msg = getMessage();
			if(!isLeader)
			{
				if(msg instanceof ForLeader)
				{
					removeMessage();
					continue;
				}
			}
			////System.out.println("recieved all");
			if(isLeader)
			{
				leader();
			}
			replica();
			acceptor();

		}
		
	}
	public void propose(Command c,int leader)
	{
		if(!decisionsReplica.containsValue(c))
		{
			for(int i=0;;i++)
			{
				if(!proposalsReplica.containsKey(i) && !decisionsReplica.containsKey(i))
				{
					proposalsReplica.put(i, c);
					ProposeMessage pMessage = new ProposeMessage(me,i,c);
					send(leader,pMessage);
					break;
				}
			}
		}
	}
	public void perform(Command c)
	{
		////System.out.println("Performing!");
		for(int i=0;i<slot_num;i++)
		{
			if(c.equals(decisionsReplica.get(i)))
			{
				slot_num++;
				return;
			}
		}
		//do something to actually perform?
		DecisionMessage d = new DecisionMessage(this.me,slot_num,c);
		
		sendAllClients(d);
		////System.out.println(this.me+" send decision to client");
		slot_num++;
	}
	
	public void sendAllClients(PaxosMessage m)
	{
		for(int p:outputs.keySet())
		{
			if(p>=totalNodes)
			{
				send(p,m);
			}
		}
	}
	public void executeInstruction( )
	{
		Instruction temp = instr;
		////System.out.println("temp "+temp);

		if(temp != null)
		{
			////System.out.println("instr "+temp.getInstr()+" num: "+temp.getNum());

			switch(temp.getInstr())
			{
				case Instruction.OPENSEND:
					int p = (Integer)(temp.getArgs()[0]);
					openSend(p);
					break;
				case Instruction.STARTSEND:
					int p1 = (Integer)(temp.getArgs()[0]);
					startSend(p1);
					break;
				case Instruction.OPENRECIEVE:
					int p2 = (Integer)(temp.getArgs()[0]);
					InetSocketAddress sa = (InetSocketAddress)(temp.getArgs()[1]);
					openRecieve(p2,sa);
					break;
				case Instruction.SEND:
					int p3 = (Integer)(temp.getArgs()[0]);
					PaxosMessage m = (PaxosMessage)(temp.getArgs()[1]);
					send(p3,m);
					break;
				case Instruction.DIE:
					die();
					break;
				case Instruction.STARTUP:
					startUp();
					break;
				case Instruction.MAKELEADER:
					makeLeader();
					break;
				default:
					super.executeInstruction();
					return;
					
			}
			if(temp.getInstr() != Instruction.STARTUP)
			{
				instr = null;
			}
			////System.out.println("Nulled "+p);
		}
	}
	
	public void makeLeader()
	{
		this.isLeader=true;
		ballot_numberLeader = new BallotNumber(0,this.me);
		Scout s = new Scout(this,scoutNum,new ProcessId(scoutNum,"scout"), this.me, acceptors, ballot_numberLeader);
		s.start();
		createConnections(s.p,s);
		s.active = true;
		scoutNum++;
		scouts.add(s);
	}
	public void startUp()
	{
		//reset all
		//System.out.println("I AM ALIVE "+ this.me);
		instr = null;
		recieved.clear();
		heartbeat=false;
		this.isLeader=false;
		this.leader=null;
		this.slot_num=1;
		this.proposalsReplica = new HashMap<Integer,Command>();
		this.proposalsLeader = new HashMap<Integer, Command>();
		this.decisionsReplica = new HashMap<Integer, Command>();
		this.ballot_numberAcceptor = null;
		this.acceptedAcceptor = new HashSet<Pvalue>();
		AliveMessage m = new AliveMessage(this.me);
		sendAllServers(m);
	}
	
	public void die()
	{
		////System.out.println("dieing");
		if(isLeader)
		{
			deLeader();
		}
		super.die();
	}
	
    public void deLeader()
    {
		for(Scout s : scouts)
		{
			s.stop();
			outputs.remove(s.p);
			inputs.remove(s.p);
		}
		for(Commander c: commanders)
		{
			c.stop();
			outputs.remove(c.p);
			inputs.remove(c.p);
		}
		scouts.clear();
		isLeader = false;
		commanders.clear();
		timeBomb = -1;
		active = false; 
		proposalsLeader.clear();
		ballot_numberLeader = new BallotNumber(0,this.me);
    }
    
    public void rePropose()
    {
    	for(int i : proposalsReplica.keySet())
    	{
    		if(!decisionsReplica.containsKey(i))
    		{
    			ProposeMessage pm = new ProposeMessage(this.me,i,proposalsReplica.get(i));
    			send(leader.getIndex(),pm);
    		}
    	}
    }
	
	
}
