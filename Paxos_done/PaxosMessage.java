import java.io.Serializable;
import java.util.*;
public class PaxosMessage implements Serializable
{
	ProcessId src;
}

class P1aMessage extends PaxosMessage
{
	BallotNumber ballot_number;
	P1aMessage(ProcessId src, BallotNumber ballot_number)
	{
		this.src = src;
		this.ballot_number = ballot_number;
	}
}

class P1bMessage extends ForLeader
{
	BallotNumber ballot_number;
	Set<Pvalue> accepted;
	int scout;
	P1bMessage(ProcessId src, BallotNumber ballot_number,Set<Pvalue> accepted,int s)
	{
		this.src = src;
		this.ballot_number = ballot_number;
		this.accepted = accepted;
		this.scout=s;
	}
}

class P2aMessage extends PaxosMessage
{
	BallotNumber ballot_number;
	int slot_number;
	Command command;
	
	P2aMessage(ProcessId src, BallotNumber ballot_number, int slot_number, Command command)
	{
		this.src=src;
		this.ballot_number = ballot_number;
		this.slot_number = slot_number;
		this.command = command;
	}
}

class P2bMessage extends ForLeader
{
	BallotNumber ballot_number;
	int slot_number;
	int commander;
	
	P2bMessage(ProcessId src, BallotNumber ballot_number, int slot_number,int comm)
	{
		this.src = src;
		this.ballot_number = ballot_number;
		this.slot_number = slot_number;
		this.commander = comm;
	}
}

class PreemptedMessage extends ForLeader
{
	BallotNumber ballot_number;
	PreemptedMessage(ProcessId src, BallotNumber ballot_number)
	{
		this.src = src;
		this.ballot_number = ballot_number;
	}
}

class AdoptedMessage extends ForLeader
{
	BallotNumber ballot_number;
	Set<Pvalue> accepted;
	AdoptedMessage(ProcessId src, BallotNumber ballot_number, Set<Pvalue> accepted)
	{
		this.src = src;
		this.ballot_number = ballot_number;
		this.accepted = accepted;
	}
}

class DecisionMessage extends PaxosMessage
{
	int slot_number;
	Command command;
	public DecisionMessage(ProcessId src, int slot_number, Command command)
	{
		this.src = src;
		this.slot_number = slot_number;
		this.command = command;
	}
}

class RequestMessage extends PaxosMessage
{
	Command command;
	public RequestMessage(ProcessId src, Command command)
	{
		this.src = src;
		this.command = command;
	}
}

class ProposeMessage extends ForLeader
{
	int slot_number;
	Command command;
	public ProposeMessage(ProcessId src, int slot_number, Command command)
	{
		this.src = src;
		this.slot_number = slot_number;
		this.command=command;
	}
}

class HeartbeatMessage extends PaxosMessage
{
	public HeartbeatMessage(ProcessId src)
	{
		this.src = src;
	}
}
class CommandDecisionMessage extends ForLeader
{
	int slot_number;
	Command command;
	public CommandDecisionMessage(ProcessId src, int slot_number, Command command)
	{
		this.src = src;
		this.slot_number = slot_number;
		this.command = command;
	}
}
class AliveMessage extends PaxosMessage
{
	public AliveMessage(ProcessId src)
	{
		this.src=src;
	}
}
class StateMessage extends PaxosMessage
{
	Map<Integer, Command> decisions;
	ProcessId leader;
	public StateMessage(ProcessId src,Map<Integer, Command> d,ProcessId leader)
	{
		this.src = src;
		this.decisions = d;
		this.leader = leader;
	}
}
class P1aMessageScout extends ForLeader
{
	BallotNumber ballot_number;
	boolean scout;
	P1aMessageScout(ProcessId src, BallotNumber ballot_number,boolean s)
	{
		this.src = src;
		this.ballot_number = ballot_number;
		scout = true;
	}
}
class P2aMessageCommander extends ForLeader
{
	BallotNumber ballot_number;
	int slot_number;
	Command command;
	
	P2aMessageCommander(ProcessId src, BallotNumber ballot_number, int slot_number, Command command)
	{
		this.src=src;
		this.ballot_number = ballot_number;
		this.slot_number = slot_number;
		this.command = command;
	}
}

class ForLeader extends PaxosMessage
{
//	public ForLeader(ProcessId src)
//	{
//		this.src = src;
//	}
}
