import java.io.Serializable;


public class Pvalue implements Serializable
{
	BallotNumber ballot_number;
	int slot_number;
	Command command;
	
	public Pvalue(BallotNumber ballot_number, int slot_number, Command command)
	{
		this.ballot_number = ballot_number;
		this.slot_number = slot_number;
		this.command = command;
	}
	public String toString()
	{
		return "PV("+ballot_number+", "+slot_number+", "+command+")";
	}
}
