﻿Allison Grabowski amg4759 allison
Noah Morrison nm22569 noahmo
We both used 1 late for a total of 4 for both of us. 

Important Note:
Our current implementation runs rather slow. If this is a problem it could be speeded up, we just didnt have time to find a sweet spot for 
recieve timeout values. 

CLASS OVERVIEW:

BallotNumber
	-holds the ProcessId of the leader whose ballot it is and its actual number
	-implements Comparable and overwrites the equals and compareTo method to perform comparisons among BallotNumber objects

ChatLogLine
	-holds the sequenceNumber, senderIndex, and message for each line of a Client's chat log
	-used to store an individual line of a client's chat log

Client
	-extends the Process class
	-used to create the client nodes
	-forever listens on its ports for a DecisionMessage from the replicas telling it about a decision made for a message and slot
	-sends a RequestMessage for a given chat message when the Master changes the value of its instr variable to CLIENTSEND

Command
	-holds the client ProcessId, overall chat message sequence number, and the actual contents of the chat message
	-implements serializable in order use the equals() method with other Command objects

Commander
	-extends the ProcessClass
	-stores the ProcessId of the leader that created it, a list of ProcessId's of acceptors and replicas, an adopted BallotNumber for its leader, a slot number and Command
	-sends a P2aMessageCommander message to its leader to pass on to the acceptors
	-waits for the leader to pass on a majority of P2bMessages from the acceptors where it will then send a CommandDecisionMessage to the leader to pass on to the replicas
	-if one of the P2bMessages passed to it by its leader shows that the leader has been preempted, send a PreemptedMessage to the leader

Instruction
	-class consisting of the enumeration for various instructions assigned to a Process's instr variable by the Master

Master
	-the master class which controls starting the system, having clients send messages, crashing and restarting servers, calling timeBombLeader, waiting for an allClear, and printing a client's chatLog

PaxosMessage
	-a general message class used for sending messages in the Paxos system
	-contains the ProcessId of the Process that sent the message
	
ForLeader
	-extends PaxosMessage
	-used for messages meant to be sent exclusively to the leader

P1aMessage
	-extends PaxosMessage
	-contains the proposed BallotNumber

P1bMessage
	-extends ForLeader
	-contains the proposed BallotNumber, the set of Pvalues, and the id index of the scout

P2aMessage
	-extends PaxosMessage
	-conatins the BallotNumber, slot number, and Command

P2bMessage
	-extends ForLeader
	-contains the BallotNumber, slot number, and the id index of the commander

PreemptedMessage
	-extends ForLeader
	-contains the BallotNumber

AdoptedMessage
	-extends ForLeader
	-contains the set of accepted Pvalues, and the BallotNumber

DecisionMessage
	-extends PaxosMEssage
	-contains the slot number and Command

RequestMessage
	-extends PaxosMessage
	-contains the Command

ProposeMessage
	-extends ForLeader
	-contains the slot number and Command

HeartbeatMessage
	-extends PaxosMessage
	-sent by the leader to all servers continuosly

CommandDecisionMessage
	-extends ForLeader
	-contains the slot number and Command

AliveMessage
	-extends PaxosMessage
	-sent to all other servers when a server is restarted

StateMessage
	-extends PaxosMessage
	-contains a server's current set of decided values and the ProcessId of who it thinks is the current leader
	-sent in response to recieving an AliveMessage

P1aMessageScout
	-extends ForLeader
	-contains the BallotNumber and a boolean value of whether is was sent by a scout

P2aMessageCommander
	-extends ForLeader
	-contains the BallotNumber, slot number, and command

Process
	-extends Thread
	-contains methods for setting up connections between threads
	-contains methods for sending and receiving messages, handles receiving HeartbeatMessage to determine when a leader is dead

ProcessId
	-contains the overall index of a process and a string specifying its type
	-implements Serializable in order to use equals() and compareTo() methods

Pvalue
	-contains a BallotNumber, slot number, and Command

Scout
	-extends Process
	-contains the ProcessId of the leader who spawned it, a list of acceptors, and a BallotNumber to try to get acceptors to adopt
	-sends a P1aMessageScout message to its leader to be passed on as a P1aMessage to all the acceptors
	-waits until the leader has passed on P1bMessages from the acceptors from a majority of acceptors containing its BallotNumber then sends an AdoptedMessage to the leader
	-unless it receives a P1bMessage with a greater BallotNumber, then it sends a PreemptedMessage to the leader

Server
	-extends Process
	-continuosly runs the replica and acceptors code as specified in Paxos Made Moderately Complex, runs the leader code as specified and sends out HeartBeatMessages if it is the leader
	-continuosly checks to see if the master has changed the value of its instr variable
	-if it is changed to DIE the process dies and sets the value of its instr variable to STARTUP to be executed when/if the master restarts it


Sending And Recieving Messages:

All send and receive code is found in the process class.
All processes have a ServerSocket per other process, which they use to write to every socket they are connected too, and a Socket per other process that they use to read from every process they are connected to. Each Socket is given a timeout, so that reads on empty sockets do not block. Sending and receiving is done by giving each process a unique int as a process number, and then storing the input and output streams for communicating with the other process are stored in a hashmap using the unique process number as a key. When a process receives a message it stores it in its message queue for later use.  If a process is instructed to send a message to itself, it simply adds the message to its own message queue. 


Master and Process Communication:
The Master and processes communicate through a shared variable called instr, that all processes have.Instr is of type Instruction, a class we created. A process is guaranteed to only write null to its instruction and the Master will block until instr is null, and then write the instruction. As part of all processes infinite executions, the instruction is regularly checked and different methods are executed based on the instruction found. It is assumed that a Master will not try to add instructions to processes it has already deemed to die. If this is done, blocking could result due to the implementation of our die method.

Leader interaction with Scout/Commander:
	Instead of setting up connections between between each newly created scout/commander with all of the servers, we simply set up a connection between a scout/commander and the leader that spawned it.  This allows the scouts and commanders to send Preempted and Adopted messages directly to the leader.  When a scout/commander needs to send messages to the entire set of acceptors or replicas, it simply sends a specified message to the leader which the leader interprets to send on to all the acceptors or replicas.  When an acceptors or replica wants to send a message to a scout or commander, it simply sends the message to the leader who passes it along to the specified scout/commander.
 
Death:
Anytime a process dies it calls the die method. This method will first call the deLeader method if the process is a leader. The deLeader method resets all variables related to being a leader and kills all scouts and commanders related to that leader. Next, the process will store the STARTUP instruction in its instruction variable and spin, reading all messages on its channels and storing them. The Master wakes up the process by changing the variable that causes the process to spin. When the process wakes up, it execute startup which clears all data related to Paxos including the received messages. 

Heartbeat and Leader Election:
Our program uses a heartbeat to implement leader election.
Every pass through the leaders body method(which occurs forever for all non-scout/commanders) has it send a heartbeat message to all servers including itself. If a process misses more than 4 consecutive heartbeats from the server it believes to be the leader, it will attempt to elect a new leader. When a process elects a new leader, it decides the leader is the next highest process number from the last leader(with wrapping such that numServers +1 = 0). However if a process receives a heartbeat message from a process that has a lower process number than its current leader, it makes that process the new leader. Thus it is possible to have multiple leaders, but eventually there will only be one.


 allClear:
We implement all clear by having the Master spin until all clients have received decision values for all chatmessages sent, and all replicas have received a decision value for the chatmessages as well.

start:
For start we create the number of servers and clients indicated. Calling start on each one so they can be ready to receive instructions from the Master. We then setup connections between all servers and all other servers, as well as all servers to all clients. Finally we set the first server created to be the leader. 


printChatLog:
to print a specified client's chat log, the master stores a copy of the client's chat log in a temporary variable.  It then goes through each entry and prints out each ChatLogLine.
