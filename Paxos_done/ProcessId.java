import java.io.Serializable;


public class ProcessId implements Comparable<Object>, Serializable
{
	int index;
	String type;
	
	public ProcessId(int index, String t)
	{
		this.index=index;
		this.type=t;
	}
	public boolean equals(Object other)
	{
		return index==((ProcessId)other).index;
	}
	public int compareTo(Object other)
	{
		return index-((ProcessId)other).index;
	}
	public String toString()
	{
		return ""+type+" "+index;
	}
	public int getIndex()
	{
		return this.index;
	}
	public String getType()
	{
		return this.type;
	}
}
