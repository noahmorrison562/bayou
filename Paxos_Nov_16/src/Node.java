import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Comparator;

import javax.swing.SwingUtilities;

public class Node extends Thread {

	public HashMap<Integer,Socket> recievers = null;
	public HashMap<Integer,Socket> others = null;

	public HashMap<Integer,ServerSocket> senders = null;
	public HashMap<Integer,ObjectInputStream> inputs = null;
	public HashMap<Integer,ObjectOutputStream> outputs = null;
	public PriorityQueue<Message> recieved = null;
	int p;
	
	
	public Node(int p,int numNodes)
	{
		super();
		this.p = p;
	    recieved = new PriorityQueue<Message>(numNodes, new Comparator<Message>() {
	        public int compare(Message m1, Message m2) {
	            if (m1.getRound() < m2.getRound())
	            {
	            	return 1;
	            }
	            else if(m1.getRound() == m2.getRound())
	            {
	            	return 0;
	            }
	            else
	            {
	            	return -1;
	            }
	        }
	    });
	    
	    recievers = new HashMap<Integer,Socket>();
	    others = new HashMap<Integer,Socket>();
		senders = new HashMap<Integer,ServerSocket>();
		inputs = new HashMap<Integer,ObjectInputStream>();
		outputs = new HashMap<Integer,ObjectOutputStream>();
	}
	
	public void run()
	{
		while(true)
		{
			recieveAll();
		}
	}
	
	public InetSocketAddress openSend(int p)
	{
		ServerSocket temp;
		SocketAddress addr;
		try {
			temp = new ServerSocket(0);
		    //temp.setSoTimeout(100);
			//System.out.println(temp.getInetAddress().getLocalHost());
			addr = temp.getLocalSocketAddress();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			temp = null;
			addr = null;
		}
		senders.put(p,temp);
		return (InetSocketAddress) addr;
		
	}
	
	public boolean startSend(final int p)
	{

		final ServerSocket s = senders.get(p);
		if(s != null)
		{

					try {
						Socket socket = s.accept();
						//System.out.println("accepted");
						others.put(p,socket);
						ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
						outputs.put(p,output);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						outputs.put(p,null);
						//socket = null;
						//System.out.println("WHAT");
						//yield();
					}
				
				
		}
		return true;

	}
	
	public boolean connected(int p)
	{
		return outputs.get(p) != null;
	}
	
	public boolean openRecieve(final int p, final InetSocketAddress adr)
	{

		if(adr == null)
		{
			return true;
		}
		//Socket temp = null;



		try {

			//System.out.println("Adr: "+ adr.getAddress().getLocalHost()+" Port: "+ adr.getPort());
			Socket temp = new Socket(adr.getAddress().getLocalHost(),adr.getPort());
			//System.out.println("Stream "+temp.getInputStream());
			ObjectInputStream input = new ObjectInputStream(temp.getInputStream());
			inputs.put(p, input);
			recievers.put(p,temp); 
			return true;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();			
			inputs.put(p, null);
			recievers.put(p,null);
			return true;
			//socket = null;
			//System.out.println("NOO");
			//yield();
		}
		
		
		


	}
	
//	public void  setOutputStream( int p,ObjectOutputStream output)
//	{
//		outputs.put(p,output);
//	}
//	
	
	public void send(int p, Message m)
	{
		ObjectOutputStream output = outputs.get(p);
		if(output != null)
		{
			try {
				output.writeObject(m);
				output.flush();
				output.reset();
				System.out.println("sent");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//do nothing
			}
		}
	}
	
	public void sendAll(Message m)
	{
		for(int p: outputs.keySet())
		{
			send(p,m);
		}
	}
	
	public void recieve(int p)
	{
		ObjectInputStream input = inputs.get(p);
		Message m = null;
		if(input != null  )
		{

			try {
				if(true)
				{

					Object o = input.readObject();
					m = (Message)o;
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				m = null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				m = null;
			}
			if(m != null)
			{
				System.out.println("Orange");
				
				recieved.add(m);
			}
			
		}
	}
	
	public void recieveAll()
	{
		for(int p: inputs.keySet())
		{
			recieve(p);
		}
	}
	
	public Message getMessage()
	{
		return recieved.poll();
	}
	
	
}

