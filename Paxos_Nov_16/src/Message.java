import java.io.Serializable;


public class Message implements  Serializable{
	
	String message;
	int round;
	int sender;
	int reciever; 
	
	public Message()
	{
		message = "NONE";
	}
	
	public Message(String s, int round, int sender, int reciever)
	{
		setMessage(s);
		setRound(round);
		setSender(sender);
		setReciever(reciever);
	}
	
	public void setMessage(String s)
	{
		message = s;
	}
	
	public void setRound(int k)
	{
		round = k;
	}
	
	public void setSender(int p)
	{
		sender = p;
	}
	public void setReciever(int p)
	{
		reciever = p;
	}
	
	public String getMessage()
	{
		return message;
	}

	public int getRound()
	{
		return round;
	}
	
	public int getSender()
	{
		return sender;
	}
	public int getReciever()
	{
		return reciever;
	}
}

