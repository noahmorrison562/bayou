import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.Scanner; 

import javax.swing.SwingUtilities;


public class Master {
	
	static String input = "CreateNodes 4\nCreateConnection 1 2\nCreateConnection 1 3\nCreateConnect 2 3\n"
			+ "Start\nSendMessage 1 2\nSendMessage 1 3\nSendMessage 1 3\n"
			+ "Print 2\nPrint 3\n"; 
  	public static void main(String [] args) throws InterruptedException
	{

		Node nodes[] = null;
		int numNodes = 0;
		String lines[] = input.split("\n");
		for(int l = 0; l < lines.length; l++)
		{
			System.out.println(lines[l]);
	  		String [] inputLine = lines[l].split(" ");
	  		int node1Index;
	  		int node2Index;
	  		Message m;
	  		switch (inputLine[0]) 
			{
	    			case "CreateNodes":
	        				/* 
	        				This commands starts this number of nodes in the
	       			 	system. These nodes should be accessible from the
	        				controller program by their index in [0,n - 1].
	         				*/
	    				System.out.println("Creating Nodes");
	    				numNodes = Integer.parseInt(inputLine[1]);
	    				nodes = new Node[numNodes];
	    				for(int i=0; i<numNodes;i++)
	    				{
	    					nodes[i] = new Node(i,numNodes);
	    				}
		
	    				//System.out.println("done creating nodes");
	        			break;
	   				case "CreateConnection":
	        				/*
	        				This command creates a bidirectional communica-
	        				tion channel through the nodes with index i and j in
	        				the system. We will call this function many times to
	        				establish the network topology before we issue any
	        				other commands.
	         				*/
	    				System.out.println("Creating Connect");

	        				final int node1I = Integer.parseInt(inputLine[1]);
	        				final int node2I = Integer.parseInt(inputLine[2]);
	        				final Node n1 = nodes[node1I];
	        				final Node n2 = nodes[node2I];
	        				final InetSocketAddress sa1 = n1.openSend(node2I);
	        				

	        				SwingUtilities.invokeLater(new Runnable()
	        				{
	        					public void run()
	        					{ 
	        						n1.startSend(node2I);
	        					}
	        				});
	        				//System.out.println("poop");
	        				n2.openRecieve(node1I, sa1);
	        				//System.out.println("poop");
	        				
	        				while(!n1.connected(node2I))
	        				{
	        					
	        				}
	        				InetSocketAddress sa2 = n2.openSend(node1I);
	        				SwingUtilities.invokeLater(new Runnable()
	        				{
	        					public void run()
	        					{ 
	        						n2.startSend(node1I);
	        					}
	        				});
	        				n1.openRecieve(node2I, sa2);
	        				while(!n2.connected(node1I))
	        				{
	        					
	        				}

					//System.out.println("done creating connections");
	        			break;
	    			case "SendMessage":
	        				/*
	        				This command sends a message from node i to node
	        				j. This operation should increment the operation
	        				counter of each node by one for the send and receive
	        				events respectively. Note that we will only call this
	        				if there is a connection between the nodes directly.
	         				*/
	    				System.out.println("Sending Message");

	        				node1Index = Integer.parseInt(inputLine[1]);
	        				node2Index = Integer.parseInt(inputLine[2]);
	        				//String message = inputLine[3];
	        				m = new Message("POOP",0, node1Index,node2Index);
	        				nodes[node1Index].send(node2Index,m);
					//System.out.println("done sending instruction to send message");
	        			break;
	    			case "LocalIncrement":
	        				/*
	       			 	This command instructs the node i to increment its
	       			 	operation counter.
	        				*/
//	        				nodeIndex = Integer.parseInt(inputLine[1]);
//					nodes[nodeIndex].increaseLocalCounter();
					//System.out.println("done local increment");
	       			break;
	    			case "MakeMasters":
	        				/*
	        				This command is used to instruct all nodes in the sys-
	        				tem that are going to be masters in the coming snap-
	        				shot. It will always be called before we call Start-
	        				Snapshot. The reason this is separate from Start-
	        				Snapshot is to ensure that you don���t mistakenly tell
	        				the masters sequentially to start the protocol because
	        				if it���s too fast, one master may do all the work. Note
	        				that you will need to inform each master node of the
	        				number of other masters so that each knows when it
	       		 		has received the snapshot information from all other
	        				masters (and therefore that the protocol is finished).
	         				*/
//	        				Controller.numMasters = inputLine.length - 1;
//	        				masterIds = new int[Controller.numMasters];
//	        				for (int i = 1; i < inputLine.length; i++) 
//					{
//	          					masterIds[i - 1] = Integer.parseInt(inputLine[i]);		
//	        				}
//					for(int i=0;i<numNodes;i++)
//					{
//						nodes[i].totalMasters=0;
//						nodes[i].master=false;
//						nodes[i].warning_color=-1;
//					}
//					for(int i=0;i<Controller.numMasters;i++)
//					{
//						nodes[masterIds[i]].totalMasters = Controller.numMasters;
//						nodes[masterIds[i]].master = true;
//					}
//					//System.out.println("done make masters");
	        			break;
	   				case "Start":
	        				/*
	        				This command initializes a snapshot with the mas-
	       				ters specified in the preceding MakeMasters com-
	        				mand. This command should block until all masters
	        				have received all snapshot information. This can be
	        				done by having the controller thread wait until it
	        				hears a confirmation message from each master.
	         				*/
					
	   					System.out.println("Starting");

						for(int i=0;i<numNodes;i++)
						{
							//nodes[i].setFinish();
							nodes[i].start();
						}	
					
	       	 		break;
	    			case "Print":
	        				/*
	        				This command instructs a node to print the full state
	        				of the most recent snapshot it partook in (in the for-
	        				mat specified in the next section). Note that this
	        				will only be called on a master node right after Start-
	        				Snapshot unblocks.
	         				*/
					//System.out.println("done print snapshot");
	    				System.out.println("Printing");

	        				int nodeIndex = Integer.parseInt(inputLine[1]);
	        				Node node = nodes[nodeIndex];
							m = node.getMessage();
							while(m != null)
							{
//								m = node.getMessage();
//								System.out.println(1);
//								if(m != null)
//								{
									System.out.println("From: " +m.getSender()+"  To: "+ m.getReciever());
									m = node.getMessage();

//								}
							}
							

					//Controller.masterCount=0;
					//System.out.println("done print snapshot");
	        			break;
	        		default:
	        			//
	        			break;
	  			}
		}
  		
	}
	
}
