
public class ChatLogLine 
{
	int sequenceNumber;
	//make so for creating clients their process id is just a number?
	ProcessId senderIndex;
	String message;
	
	public ChatLogLine(int sn, ProcessId client, String m)
	{
		sequenceNumber = sn;
		senderIndex = client;
		message = m;
	}
	public String toString()
	{
		if(!message.equals("NOP"))
		{
			return ""+sequenceNumber+" "+senderIndex+": "+message+"\n";
		}
		else
		{
			return "";
		}
	}
}
