import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class Server extends Process 
{
	boolean isLeader;
	boolean acceptor;
	boolean replica;
	ProcessId ProcessId;
	int scoutNum;
	int quorum;
	int slot_num=1;
	Map<Integer, Command> proposalsReplica = new HashMap<Integer,Command>();
	Map<Integer, Command> proposalsLeader = new HashMap<Integer, Command>();
	Map<Integer, Command> decisionsReplica = new HashMap<Integer, Command>();
	BallotNumber ballot_numberAcceptor = null;
	Set<Pvalue> acceptedAcceptor = new HashSet<Pvalue>();
	ProcessId[] acceptors;
	ProcessId[] replicas;
	
	volatile int timeBomb=-1;
	Server(int p, boolean leader, boolean acceptor,boolean replica,int q,ProcessId l,ProcessId m,int i,int sNum)
	{
		super(p);
		this.isLeader = leader;
		this.acceptor = acceptor;
		this.replica = replica;
		this.scoutNum = sNum;
		this.quorum=q;
		this.totalNodes = i;
		ProcessId[] a = new ProcessId[this.totalNodes];
		ProcessId[] r = new ProcessId[this.totalNodes];
		for(int f=0;f<this.totalNodes;f++)
		{
			a[f] = new ProcessId(f,"server");
			r[f] = new ProcessId(f,"server");
		}
		this.acceptors = a;
		this.replicas = r;
		this.leader = l;
		this.me = m;
	}
	public void leader()
	{
		boolean active = false;
		BallotNumber ballot_number = new BallotNumber(0,this.me);
		
		new Scout(this,scoutNum,new ProcessId(scoutNum,"scout"), this.me, acceptors, ballot_number);
		scoutNum++;
			//get next message
			PaxosMessage msg = getMessage();
			if(msg instanceof ProposeMessage)
			{
				ProposeMessage m = (ProposeMessage) msg;
				if(!proposalsLeader.containsKey(m.slot_number))
				{
					proposalsLeader.put(m.slot_number, m.command);
					if(active)
					{
						
						new Commander(this,scoutNum,new ProcessId(scoutNum,"commander"),me,acceptors,replicas,ballot_number,m.slot_number,m.command);
						scoutNum++;
					}
				}
			}
			else if(msg instanceof AdoptedMessage)
			{
				AdoptedMessage m = (AdoptedMessage) msg;
				if(ballot_number.equals(m.ballot_number))
				{
					Map<Integer, BallotNumber> max = new HashMap<Integer, BallotNumber>();
					for(Pvalue pv: m.accepted)
					{
						BallotNumber bn = max.get(pv.slot_number);
						if(bn==null || bn.compareTo(pv.ballot_number)<0)
						{
							max.put(pv.slot_number,pv.ballot_number);
							proposalsLeader.put(pv.slot_number, pv.command);
						}
					}
					for(int sn: proposalsLeader.keySet())
					{
						
						new Commander(this,scoutNum,new ProcessId(scoutNum,"commander"),me,acceptors, replicas,ballot_number,sn,proposalsLeader.get(sn));
						scoutNum++;
					}
					active=true;
				}
			}
			else if(msg instanceof PreemptedMessage)
			{
				PreemptedMessage m = (PreemptedMessage) msg;
				if(ballot_number.compareTo(m.ballot_number)<0)
				{
					ballot_number = new BallotNumber(m.ballot_number.round+1,me);
					
					new Scout(this,scoutNum,new ProcessId(scoutNum,"scout"),me,acceptors,ballot_number);
					scoutNum++;
					active = false;
				}
			}
			else if(msg instanceof P2aMessage)
			{
				//message from the commander to send to all the acceptors
				sendAllServers(msg);
			}
			else if(msg instanceof P2bMessage)
			{
				//P2bMessage to send to the commander
				P2bMessage m = (P2bMessage)msg;
				send(m.commander,msg);
			}
			else if(msg instanceof P1aMessage)
			{
				//message from the scout to send to all acceptors
				sendAllServers(msg);
			}
			else if(msg instanceof P1bMessage)
			{
				//message to send to the scout
				P1bMessage m = (P1bMessage)msg;
				send(m.scout,msg);
			}
			else if(msg instanceof CommandDecisionMessage)
			{
				CommandDecisionMessage cdm = (CommandDecisionMessage)msg;
				DecisionMessage dm = new DecisionMessage(cdm.src,cdm.slot_number,cdm.command);
				sendAllServers(dm);
			}
			else
			{
				System.err.println("Leader: unknown msg type");
			}
		
	}
	public void acceptor()
	{
		
		//add me as acceptor connector to send and recieve messages
		//receive next message
		PaxosMessage msg = getMessage();
		if(msg instanceof P1aMessage)
		{
			P1aMessage m = (P1aMessage) msg;
			if(ballot_numberAcceptor ==null || ballot_numberAcceptor.compareTo(m.ballot_number)<0)
			{
				ballot_numberAcceptor = m.ballot_number;
			}
			//send P1b message to m.src
			P1bMessage p1bMessage = new P1bMessage(this.me,ballot_numberAcceptor,acceptedAcceptor,m.src.getIndex());
			send(this.leader.getIndex(),p1bMessage);
		}
		else if(msg instanceof P2aMessage)
		{
			P2aMessage m = (P2aMessage) msg;
			
			if(ballot_numberAcceptor==null || ballot_numberAcceptor.compareTo(m.ballot_number)<=0)
			{
				ballot_numberAcceptor = m.ballot_number;
				acceptedAcceptor.add(new Pvalue(ballot_numberAcceptor, m.slot_number,m.command));
			}
			//send P2bMessage to m.src
			P2bMessage p2bMessage = new P2bMessage(this.me,ballot_numberAcceptor,m.slot_number,m.src.getIndex());
			send(this.leader.getIndex(),p2bMessage);
		}
	}
	public void replica()
	{
		
			//receive next message
			PaxosMessage msg = getMessage();
			
			if(msg instanceof RequestMessage)
			{
				RequestMessage m = (RequestMessage) msg;
				Command c = m.command;
				//propose method start
				propose(c,leader.getIndex());
				//propose method end
			}
			else if(msg instanceof DecisionMessage)
			{
				DecisionMessage m = (DecisionMessage) msg;
				decisionsReplica.put(m.slot_number,  m.command);
				for(;;)
				{
					Command c = decisionsReplica.get(slot_num);
					if(c==null)
					{
						break;
					}
					Command c2 = proposalsReplica.get(slot_num);
					if(c2!=null && !c2.equals(c))
					{
						//propose start
						propose(c2,leader.getIndex());
						//propose end
					}
					// perform c, have to check what this entails
					perform(c);
				}
			}
			else
			{
				System.err.println("Replica: unkown message type");
			}
		
	}
	public void body()
	{
		while(true)
		{
			if(isLeader)
			{
				sendAllServers(new HeartbeatMessage(this.me));
			}
			recieveAll();
			if(isLeader)
			{
				leader();
			}
			replica();
			acceptor();
		}
		
	}
	public void propose(Command c,int leader)
	{
		if(!decisionsReplica.containsValue(c))
		{
			for(int i=1;;i++)
			{
				if(!proposalsReplica.containsKey(i) && !decisionsReplica.containsKey(i))
				{
					proposalsReplica.put(i, c);
					ProposeMessage pMessage = new ProposeMessage(me,i,c);
					send(leader,pMessage);
					break;
				}
			}
		}
	}
	public void perform(Command c)
	{
		for(int i=1;i<slot_num;i++)
		{
			if(c.equals(decisionsReplica.get(i)))
			{
				slot_num++;
				return;
			}
		}
		//do something to actually perform?
		DecisionMessage d = new DecisionMessage(this.me,slot_num,c);
		sendAllClients(d);
		slot_num++;
	}
	public void sendAllServers(PaxosMessage m)
	{
			for(int p: outputs.keySet())
			{
				if(p<totalNodes)
				{
					send(p,m);
				}
				
			}
	}
	public void sendAllClients(PaxosMessage m)
	{
		for(int p:outputs.keySet())
		{
			if(p>=totalNodes)
			{
				send(p,m);
			}
		}
	}
}
