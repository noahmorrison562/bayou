import java.util.*;
public class Commander extends Process
{
	ProcessId leader;
	ProcessId[] acceptors, replicas;
	BallotNumber ballot_number;
	int slot_number;
	Command command;
	Server lead;
	boolean active = false;
	
	public Commander(Server s,int p,ProcessId me, ProcessId leader, ProcessId[] acceptors, ProcessId[] replicas, BallotNumber ballot_number, int slot_number, Command command)
	{
		super(p);
		System.out.println("New Commander!");

		this.me = me;
		this.acceptors = acceptors;
		this.replicas =replicas;
		this.leader = leader;
		this.ballot_number = ballot_number;
		this.slot_number = slot_number;
		this.command = command;
		this.lead = s;
		createConnections(this.lead.p,this.lead);
		//set up connections so commander can send and receive messages
	}
	
	public void run()
	{
		while(!this.active)
		{
			executeInstruction();
		}
		
		body();
		
	}
	
	public void body()
	{
		P2aMessage m2 = new P2aMessage(me, ballot_number, slot_number, command);
		Set<ProcessId> waitFor = new HashSet<ProcessId>();
		// for all a in acceptors, send m2 to a and add a to the waitFor set
		send(leader.getIndex(),m2);
		for(ProcessId a:acceptors)
		{
			waitFor.add(a);
		}
		while(2*waitFor.size() >= acceptors.length)
		{
			//receive next message
			recieveAll();
			PaxosMessage msg = getMessage();
			if(msg instanceof P2bMessage)
			{
				P2bMessage m = (P2bMessage) msg;
				
				if(ballot_number.equals(m.ballot_number))
				{
					if(waitFor.contains(m.src))
					{
						waitFor.remove(m.src);
					}
				}
				else
				{
					//send preempted message to the leader
					PreemptedMessage preempt = new PreemptedMessage(this.me,m.ballot_number);
					send(leader.getIndex(),preempt);
					return;
				}
			}
		}
		//for all r in replicas, send decision message to the replicas
		//for(ProcessId r: replicas)
		//{
			CommandDecisionMessage dMessage = new CommandDecisionMessage(this.me,slot_number,command);
			send(this.leader.getIndex(),dMessage);
		//}
	}
}
