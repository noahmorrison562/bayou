import java.io.Serializable;

//need to change so that each command contains the chat message since thats really the only
//command
public class Command implements Serializable
{
	ProcessId client;
	int req_id;
	String chatMessage;
	
	public Command(ProcessId client, int req_id, String op)
	{
		this.client = client;
		this.req_id = req_id;
		this.chatMessage = op;
	}
	public boolean equals(Object o)
	{
		Command other = (Command) o;
		return client.equals(other.client) && req_id==other.req_id && chatMessage.equals(other.chatMessage);
	}
	public String toString()
	{
		return "Command("+client+", "+req_id+", "+chatMessage+")";
	}
}
