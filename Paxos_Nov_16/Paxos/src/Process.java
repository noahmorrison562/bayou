import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import javax.swing.SwingUtilities;


public class Process extends Thread
{
	public HashMap<Integer,Socket> recievers = null;
	public HashMap<Integer,ServerSocket> senders = null;
	public HashMap<Integer,ObjectInputStream> inputs = null;
	public HashMap<Integer,ObjectOutputStream> outputs = null;
	public Queue<PaxosMessage> recieved = null;
	int p;
	boolean heartbeat=false;
	volatile public Instruction instr = null;
	int totalNodes;
	ProcessId leader;
	ProcessId me;
	public Process(int p)
	{
		this.p=p;
		recievers = new HashMap<Integer,Socket>();
		senders = new HashMap<Integer,ServerSocket>();
		inputs = new HashMap<Integer,ObjectInputStream>();
		outputs = new HashMap<Integer,ObjectOutputStream>();
		recieved = new LinkedList<PaxosMessage>();
	}
	public void body()
	{
	
	}
	public void run()
	{
		body();
	}
	public void executeInstruction( )
	{
		Instruction temp = instr;
		//System.out.println(temp+" Instr "+this.p);
		if(temp != null)
		{
			//System.out.println("instr "+temp.getInstr()+" num: "+temp.getNum());

			switch(temp.getInstr())
			{
				case Instruction.SELFCONNECT:
					connectSelf();
					break;
				case Instruction.OPENSEND:
					int p = (Integer)(temp.getArgs()[0]);
					openSend(p);
					break;
				case Instruction.STARTSEND:
					int p1 = (Integer)(temp.getArgs()[0]);
					startSend(p1);
					break;
				case Instruction.OPENRECIEVE:
					int p2 = (Integer)(temp.getArgs()[0]);
					InetSocketAddress sa = (InetSocketAddress)(temp.getArgs()[1]);
					openRecieve(p2,sa);
					break;
				case Instruction.SEND:
					int p3 = (Integer)(temp.getArgs()[0]);
					PaxosMessage m = (PaxosMessage)(temp.getArgs()[1]);
					send(p3,m);
					break;
				case Instruction.PRINT:
					print();
					break;
				case Instruction.DIE:
					die();
					break;
				case Instruction.STARTUP:
					startUp();
					break;
				case Instruction.CLIENTSEND:
					RequestMessage m1=(RequestMessage)(temp.getArgs()[0]);
					sendAll(m1);
					break;
				default:
					//do nothing
					break;
			}
			if(temp.getInstr() != Instruction.STARTUP)
			{
				instr = null;
				//System.out.println("nulled "+p);

			}
			//System.out.println("Nulled "+p);
		}
	}
	
	@SuppressWarnings("deprecation")
	public void die()
	{
		Instruction start = new Instruction(Instruction.STARTUP,null);
		instr = start;
		suspend();
		executeInstruction();
		
	}
	
	public void startUp()
	{
		//reset all
		instr = null;
		heartbeat=false;
		AliveMessage m = new AliveMessage(this.me);
		sendAllServers(m);
	}
	public void sendAllServers(PaxosMessage m)
	{
			for(int p: outputs.keySet())
			{
				if(p<totalNodes)
				{
					send(p,m);
				}
				
			}
	}
	public InetSocketAddress openSend(int p)
	{
		System.out.println("openSend");

		ServerSocket temp;
		SocketAddress addr;
		try {
			temp = new ServerSocket(0);
			addr = temp.getLocalSocketAddress();
			senders.put(p,temp);
			//System.out.println("putted");
			Socket socket = temp.accept();		
			ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
			outputs.put(p,output);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			temp = null;
			addr = null;
			outputs.put(p,null);
			senders.put(p,null);
		}
		return (InetSocketAddress) addr;
		
	}
	
	public boolean startSend(final int p)
	{

		final ServerSocket s = senders.get(p);
		if(s != null)
		{

					try {
						Socket socket = s.accept();
						//System.out.println("accepted");
						ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
						outputs.put(p,output);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						outputs.put(p,null);
						//socket = null;
						//System.out.println("WHAT");
						//yield();
					}
				
				
		}
		return true;


	}
	
	public boolean connected(int p)
	{
		return outputs.get(p) != null;
	}
	
	public boolean openRecieve(final int p, final InetSocketAddress adr)
	{

		if(adr == null)
		{
			return true;
		}
		//Socket temp = null;



		try {

			//System.out.println("Adr: "+ adr.getAddress().getLocalHost()+" Port: "+ adr.getPort());
			Socket temp = new Socket(adr.getAddress().getLocalHost(),adr.getPort());
			//System.out.println("Stream "+temp.getInputStream());
			ObjectInputStream input = new ObjectInputStream(temp.getInputStream());
			inputs.put(p, input);
			recievers.put(p,temp); 
			temp.setSoTimeout(1000);
			return true;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();			
			inputs.put(p, null);
			recievers.put(p,null);
			return true;
			//socket = null;
			//System.out.println("NOO");
			//yield();
		}
		
		
		


	}
	
//	public void  setOutputStream( int p,ObjectOutputStream output)
//	{
//		outputs.put(p,output);
//	}
//	
	
	public void send(int p, PaxosMessage m)
	{
		if(p==this.p)
		{
			recieved.add(m);
		}
		else
		{
			ObjectOutputStream output = outputs.get(p);
			if(output != null)
			{
				try {
					output.writeObject(m);
					output.flush();
					output.reset();
					System.out.println("sent "+this.p+" : "+p);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					//do nothing
				}
			}
		}
		
	}
	
	public void sendAll(PaxosMessage m)
	{
		for(int p: outputs.keySet())
		{
			send(p,m);
		}
	}
	
	public void connectSelf()
	{
		outputs.put(this.p,null);
	}
	
	public void recieve(int p)
	{
		ObjectInputStream input = inputs.get(p);
		PaxosMessage m = null;
		if(input != null  )
		{

			try {
				if(p!=this.p)
				{
					Object o = input.readObject();
					m = (PaxosMessage)o;
					if(m instanceof HeartbeatMessage)
					{
						heartbeat=true;
					}
				}
				else if(input.available()>0)
				{
					Object o = input.readObject();
					m = (PaxosMessage)o;
					if(m instanceof HeartbeatMessage)
					{
						heartbeat=true;
					}
					else
					{
						recieved.add(m);
					}
				}
					
			} catch(java.net.SocketTimeoutException e)
			{
				m=null;
			}
			catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(this);
				m = null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				m = null;
			}
		}
	}
	
	public void recieveAll()
	{
		for(int p: inputs.keySet())
		{
			recieve(p);
		}
		if(!heartbeat && this.me.getType().equals("server"))
		{
			electLeader();
		}
		else
		{
			heartbeat=false;
		}
	}
	
	public PaxosMessage getMessage()
	{
		return recieved.peek();
	}
	public void removeMessage()
	{
		recieved.poll();
	}
    //assuming can share memory
    public void createConnections(int P2I,Process p2)
      {
        final int Process1I = this.p;
        final int Process2I = P2I;
        final Process n1 = this;
        final Process n2 = p2;
        
        
        ServerSocket temp;
        SocketAddress addr;
        try {
            temp = new ServerSocket(0);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            outputs.put(p,null);
            senders.put(p,null);
            return;

        }
        InetSocketAddress sa1 = (InetSocketAddress)temp.getLocalSocketAddress();
        Object args2[] = new Object[2];
        args2[0] = (Integer)this.p;
        args2[1] = sa1;
        Instruction i2 = new Instruction(Instruction.OPENRECIEVE,args2);
        putInstruction(n2, i2);
        //System.out.println("I2");
        

        try {
            senders.put(p,temp);
            Socket socket;
            socket = temp.accept();
            ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
            
            outputs.put(p,output);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            outputs.put(p,null);
            senders.put(p,null);
            return;
        }        
        
        Object args3[] = new Object[1];
        args3[0] = (Integer)this.p;
        Instruction i3 = new Instruction(Instruction.OPENSEND,args3);
        putInstruction(n2,i3);
        InetSocketAddress sa2 = getAddress(n2,this.p);
        //System.out.println("I3");

        
        
        //n1.startSend(Process2I);
        Object args4[] = new Object[2];
        openRecieve(Process2I,sa2);
        //System.out.println("I4");

      }
	public void print()
	{
		P2bMessage m1 = (P2bMessage)getMessage();
		while(m1 != null)
		{
//			m = Process.getPaxosMessage();
//			System.out.println(1);
//			if(m != null)
//			{
				System.out.println("From: " +m1.src+"  To: "+ m1.slot_number);
				m1 = (P2bMessage)getMessage();

//			}
		}
	}
	public void electLeader()
	{
		int newLeader = leader.getIndex()+1;
		if(newLeader==totalNodes)
		{
			newLeader=0;
		}
		leader = new ProcessId(newLeader,"server");
	}
	public static void putInstruction(Process p, Instruction instruction)
    {
        while(p.instr != null)
        {
            
        }
        //System.out.println("Instruction sent " + instruction.getNum());
        p.instr = instruction;
    }
    
    public static InetSocketAddress getAddress(Process p, int n2)
    {
        InetSocketAddress sa1 = null;
        
        while(sa1 == null)
        {
            ServerSocket s = p.senders.get(n2);
            if(s != null)
            {
                sa1 = (InetSocketAddress)s.getLocalSocketAddress();
            }
        }
        return sa1;
    }
    
    public static boolean connected(int n,Process p)
    {
        return p.outputs.get(n) != null;
    }

}
