import java.util.HashSet;
import java.util.Set;


public class Scout extends Process 
{
	ProcessId leader;
	ProcessId[] acceptors;
	BallotNumber ballot_number;
	Server lead;
	boolean active = false;
	public Scout(Server s,int p,ProcessId me, ProcessId leader, ProcessId[] acceptors, BallotNumber ballot_number)
	{
		super(p);
		System.out.println("New Scout!");
		this.me = me;
		this.acceptors = acceptors;
		this.leader = leader;
		this.ballot_number = ballot_number;
		this.lead = s;
		
		//set up connections so scout can send and recieve messages
		//
	}
	public void body()
	{
		P1aMessage m1 = new P1aMessage(me,ballot_number);
		Set<ProcessId> waitFor = new HashSet<ProcessId>();
		//for all a in acceptors, send m1 to a, add a to waitfor set
		send(leader.getIndex(),m1);
		for(ProcessId a: acceptors)
		{
			waitFor.add(a);
		}
		Set<Pvalue> pvalues = new HashSet<Pvalue>();
		
		while(2*waitFor.size() >=acceptors.length)
		{
			recieveAll();
			//receive message and store in
			PaxosMessage msg = getMessage();
			if(msg instanceof P1bMessage)
			{
				P1bMessage m = (P1bMessage)msg;
				int cmp = ballot_number.compareTo(m.ballot_number);
				if(cmp!=0)
				{
					//send a preempted message to the leader
					PreemptedMessage preMessage = new PreemptedMessage(this.me,ballot_number);
					send(leader.getIndex(),preMessage);
					return;
				}
				if(waitFor.contains(m.src))
				{
					waitFor.remove(m);
					pvalues.addAll(m.accepted);
				}
			}
			else if(msg != null)
			{
				System.err.println("Scout: unexpected message");
			}
		}
		//send an adopted message to the leader
		AdoptedMessage adoptMessage = new AdoptedMessage(this.me,ballot_number,pvalues);
		send(leader.getIndex(),adoptMessage);
	}
	public void run()
	{
		while(!this.active)
		{
			executeInstruction();
		}
		
		body();
		
	}
	
}
