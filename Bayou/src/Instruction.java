public class Instruction {

	int msgNum;
	int instruction;
	Object args[];
	public final static int OPENSEND = 0;
	public final static int CLOSESEND = 1;
	public final static int OPENRECIEVE = 2;
	public final static int SEND = 3;
	public final static int PRINT = 4; 
	public final static int DIE = 5;
	public final static int RECONNECT = 6;
	public final static int CLOSERECEIVE = 7;
	public final static int RETIRE = 8;
	public final static int PUT = 9;
	public final static int DELETE = 10;
	public final static int GET = 11;
	public final static int CREATE = 12;
	public final static int PAUSE = 13;
	public final static int START = 14;
	public final static int STABLE = 15;


	private static int instrNum = 0;
	
	
	public Instruction(int inst,Object args[])
	{
		msgNum = instrNum;
		instrNum++;
		instruction = inst;
		this.args = args;
	}
	
	public int getNum()
	{
		return msgNum;
	}
	
	public int getInstr()
	{
		return instruction;
	}
	
	public Object[] getArgs()
	{
		return args;
	}
	
			
	
}