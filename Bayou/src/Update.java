import java.io.Serializable;


public class Update implements Serializable, Comparable{
	String songName;
	String oldURL;
	String newURL;
	AcceptStamp aStamp;
	int cStamp;
	boolean creation;
	boolean retirement;
	
	public Update(String sn,String ou, String nu, AcceptStamp n,int cs,boolean c,boolean r)
	{
		songName = sn;
		oldURL = ou;
		newURL = nu;
		aStamp = n;
		cStamp = cs;
		creation = c;
		retirement=r;
	}
	public boolean equals(Object other)
	{
		
		
		if(songName==null)
		{
			return aStamp.equals(((Update)other).aStamp)
			&& cStamp==((Update)other).cStamp
			&& creation==((Update)other).creation
			&& retirement==((Update)other).retirement;
		}
		return songName.equals(((Update)other).songName)
				&& aStamp.equals(((Update)other).aStamp)
				&& cStamp==((Update)other).cStamp
				&& creation==((Update)other).creation
				&& retirement==((Update)other).retirement;
	}
	public String toString()
	{
		String s="";
		if(songName==null)
		{
			s="CREATION\n";
		}
		else
		{
			if(newURL==null)
			{
				s+="DELETE:(";
				s+=songName+"):";
			}
			else
			{
				s+="PUT:(";
				s+=songName+", "+newURL+"):";
			}
			if(cStamp!=-1)
			{
				s+="TRUE\n";
			}
			else
			{
				s+="FALSE\n";
			}
		}
		return s;
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
}


