import java.io.Serializable;


public class ProcessId implements Comparable<Object>, Serializable
{
	int index;
	String type;
	AcceptStamp creationAcceptStamp;
	ProcessId creator;
	
	public ProcessId(int index, String t, AcceptStamp c, ProcessId cr)
	{
		this.index=index;
		this.type=t;
		this.creationAcceptStamp = c;
		this.creator = cr;
	}
	public boolean equals(Object other)
	{
		return index==((ProcessId)other).index && 
				type.equals(((ProcessId)other).type) && 
				creator.equals(((ProcessId)other).creator) &&
				creationAcceptStamp.equals(((ProcessId)other).creationAcceptStamp);
	}
	public int compareTo(Object other)
	{	
		if(creator==null&&((ProcessId)other).creator==null)
		{
			return creationAcceptStamp.acceptTime-((ProcessId)other).creationAcceptStamp.acceptTime;
		}
		else if(creator.equals(((ProcessId)other).creator))
		{
			return creationAcceptStamp.acceptTime-((ProcessId)other).creationAcceptStamp.acceptTime;
		}
		else
		{
			if(creator==null)
			{
				return -1;
			}
			if(((ProcessId)other).creator==null)
			{
				return 1;
			}
			return creator.compareTo(((ProcessId)other).creator);
		}
	}
	public String toString()
	{
		return ""+index+" "+type+" "+creationAcceptStamp+" creator("+creator+")";
	}
	public int getIndex()
	{
		return this.index;
	}
	public String getType()
	{
		return this.type;
	}
}
