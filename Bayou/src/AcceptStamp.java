import java.io.Serializable;


public class AcceptStamp implements Comparable<Object>, Serializable{
	int acceptTime;
	ProcessId replicaId;
	
	public AcceptStamp(int a, ProcessId r)
	{
		acceptTime = a;
		replicaId = r;
	}

	public boolean equals(Object other)
	{
		return replicaId.compareTo(((AcceptStamp)other).replicaId)==0 && acceptTime==((AcceptStamp)other).acceptTime;
	}
	public int compareTo(Object other)
	{
		return acceptTime-((AcceptStamp)other).acceptTime;
	}
	
	public String toString()
	{
		return "("+acceptTime+", "+")";
	}
}
