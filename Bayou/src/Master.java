import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

public class Master {
    static ProcessId primary = null;
    static HashMap<Integer,Server> servers = new HashMap<Integer,Server>();
    static HashMap<Integer,Client> clients = new HashMap<Integer,Client>();
    static HashMap<Integer,Server> connected = new HashMap<Integer,Server>();
    static HashMap<Integer,Process> processes = new HashMap<Integer,Process>();
    public final static String defaultURL = "ERR_KEY";
    
  public static void main(String [] args) {
	  System.out.println("started");
    Scanner scan = new Scanner(System.in);
    while (scan.hasNextLine()) {
      String [] inputLine = scan.nextLine().split(" ");
      int clientId, serverId, id1, id2;
      String songName, URL;
      ProcessId pid1,pid2;
      Process p1,p2;
      Server tempS;
      Client tempC;
      Object[] arg;
      Instruction instr;
      switch (inputLine[0]) {
        case "joinServer":
        	System.out.println("starting join server");
            serverId = Integer.parseInt(inputLine[1]);
            pid1 = new ProcessId(serverId,"server",null,null);
            if(primary == null)
            {
            	primary = pid1;
            	tempS = new Server(pid1,primary,true);
            }
            else
            {
            	System.out.println("starting non primary server");
            	tempS = new Server(pid1,primary,false);
            }
            tempS.start();
            connectAllServers(tempS);
            System.out.println("finished connect all servers");
            servers.put(serverId,tempS);
            connected.put(serverId,tempS);
            processes.put(serverId, tempS);
            System.out.println("finished putting stuff in sets");
            /*
             * Start up a new server with this id and connect it to all servers
             */
            Instruction createI = new Instruction(Instruction.CREATE,null);
            putInstruction(tempS,createI);
            System.out.println("starting server: "+serverId);
            
            while(tempS.pause)
            {
            	
            }
            System.out.println("ended join server");
            break;
        case "retireServer":
            serverId = Integer.parseInt(inputLine[1]);
	        Instruction retireI = new Instruction(Instruction.RETIRE,null);
	        tempS = servers.get(serverId);
			putInstruction(tempS,retireI);
			while(!tempS.dead)
			{
				
			}
            for(int n : processes.keySet())
            {
            	breakConnection(processes.get(n),processes.get(serverId));
            }
	    /*
	     * Retire the server with the id specified. This should block until
	     * the server can tell another server of its retirement
             */ 
            break;
        case "joinClient":
        	System.out.println("starting join client");
            clientId = Integer.parseInt(inputLine[1]);
            serverId = Integer.parseInt(inputLine[2]);
            pid1 = new ProcessId(clientId,"client",null,null);
            tempC = new Client(pid1);
            tempC.start();
            createConnections(tempC,connected.get(serverId));
            clients.put(clientId, tempC);
            processes.put(clientId, tempC);
            System.out.println("ending join client");
            /*
             * Start a new client with the id specified and connect it to 
	     * the server
             */
            break;
        case "breakConnection":
	    id1 = Integer.parseInt(inputLine[1]);
	    id2 = Integer.parseInt(inputLine[2]);
	    
	    breakConnection(processes.get(id1),processes.get(id2));
	    
            /*
             * Break the connection between a client and a server or between
	     * two servers
             */
            break;
	case "restoreConnection":
	    id1 = Integer.parseInt(inputLine[1]);
	    id2 = Integer.parseInt(inputLine[2]);
	    createConnections(processes.get(id1),processes.get(id2));
            /*
             * Restore the connection between a client and a server or between
	     * two servers
             */
            break;
        case "pause":
            /*
             * Pause the system and don't allow any Anti-Entropy messages to
	     * propagate through the system
             */
        	Instruction pauseI = new Instruction(Instruction.PAUSE,null);
        	for(int n:servers.keySet())
        	{
        		tempS = servers.get(n);
        		putInstruction(tempS,pauseI);
        	}
            break;
        case "start":
            /*
             * Resume the system and allow any Anti-Entropy messages to
	     * propagate through the system
	     */
        	Instruction startI = new Instruction(Instruction.START,null);
        	for(int n:servers.keySet())
        	{
        		tempS = servers.get(n);
        		putInstruction(tempS,startI);
        	}
            break;
	case "stabilize":
            /*
             * Block until there are enough Anti-Entropy messages for all values to 
             * propagate through the currently connected servers. In general, the 
             * time that this function blocks for should increase linearly with the 
	     * number of servers in the system.
	     */
		//just have us sleep here?
			Instruction startS = new Instruction(Instruction.STABLE,null);
			for(int n:servers.keySet())
	    	{
	    		tempS = servers.get(n);
	    		putInstruction(tempS,startS);
	    	}
			boolean breakOut = true;
			do
			{
				breakOut=true;
				for(int n:servers.keySet())
		    	{
		    		tempS = servers.get(n);
		    		if(tempS.instr!=null)
		    		{
		    			breakOut = false;
		    			continue;
		    		}
		    		if(tempS.stable==true)
		    		{
		    			breakOut = false;
		    		}
		    	}
			}
			while(!breakOut);
			System.out.println("finsihed stabilze");
            break;
        case "printLog":
        	System.out.println("start print log");
            serverId = Integer.parseInt(inputLine[1]);
            /*
             * Print out a server's operation log in the format specified in the
	     * handout.
	     */
            instr = new Instruction(Instruction.PRINT,null);
            tempS = servers.get(serverId);
            putInstruction(tempS,instr);
            System.out.println("finished printing log");
            break;
        case "put":
        	System.out.println("start client put");
            clientId = Integer.parseInt(inputLine[1]);
		    songName = inputLine[2];
		    URL = inputLine[3];
		    arg = new Object[2];
		    arg[0] = songName;
		    arg[1] = URL;
		    tempC = clients.get(clientId);
		    instr =  new Instruction(Instruction.PUT,arg);
		    putInstruction(tempC,instr);
		    while(!serverReached(tempC,instr.getNum()))
		    {
		    	//spin
		    }
	    	System.out.println("end client put");
            /*
             * Instruct the client specified to associate the given URL with the given
	     * songName. This command should block until the client communicates with
	     * one server.
             */ 
            break;
	case "get":
            clientId = Integer.parseInt(inputLine[1]);
		    songName = inputLine[2];
		    arg = new Object[1];
		    arg[0] = songName;
		    tempC = clients.get(clientId);
		    instr =  new Instruction(Instruction.GET,arg);
		    putInstruction(tempC,instr);
		    while(!serverReached(tempC,instr.getNum()))
		    {
		    	//spin
		    }
		    System.out.println(songName+":"+getURL(tempC,instr.getNum()));
            /*
             * Instruct the client specified to attempt to get the URL associated with
	     * the given songName. The value should then be printed to standard out of 
	     * the master script in the format specified in the handout. This command 
	     * should block until the client communicates with one server.
	     */ 
            break;
        case "delete":
            clientId = Integer.parseInt(inputLine[1]);
            songName = inputLine[2];
            
    	    arg = new Object[1];
    	    arg[0] = songName;
    	    tempC = clients.get(clientId);
    	    instr =  new Instruction(Instruction.DELETE,arg);
    	    putInstruction(tempC,instr);
    	    while(!serverReached(tempC,instr.getNum()))
    	    {
    	    	//spin
    	    }
            /*
             * Instruct the client to delete the given songName from the playlist. 
             * This command should block until the client communicates with one server.
             */ 
            break;
      }
    }
    System.exit(0);
  }
  
  public static void createConnections(Process p1, Process p2)
	{
	  	int P1I = p1.me.index;
	  	int P2I = p2.me.index;
	  	if(P1I == P2I)
	  	{
	  		return;
	  	}
		final int Process1I = P1I;
		final int Process2I = P2I;
		final Process n1 = p1;
		final Process n2 = p2;
		
		Object args1[] = new Object[1];
		args1[0] = (Integer)P2I;
		Instruction i1 = new Instruction(Instruction.OPENSEND,args1);
		putInstruction(n1,i1);
		InetSocketAddress sa1 = getAddress(n1,P2I);

		Object args2[] = new Object[2];
		args2[0] = (Integer)P1I;
		args2[1] = sa1; 
		Instruction i2 = new Instruction(Instruction.OPENRECIEVE,args2);
		putInstruction(n2, i2);
		
		Object args3[] = new Object[1];
		args3[0] = (Integer)P1I;
		Instruction i3 = new Instruction(Instruction.OPENSEND,args3);
		putInstruction(n2,i3);
		InetSocketAddress sa2 = getAddress(n2,P1I);

		
		
		Object args4[] = new Object[2];
		args4[0] = (Integer)P2I;
		args4[1] = sa2; 
		Instruction i4 = new Instruction(Instruction.OPENRECIEVE,args4);
		putInstruction(n1, i4);
		System.out.println("finished creating a connection");
	}
  
	
	public static void putInstruction(Process p, Instruction instruction)
	{
		while(p.instr != null)
		{
		}
		p.instr = instruction;
		System.out.println("successfully put instruction:"+instruction.getInstr()+" in process "+p.me.index);
	}
	
	public static InetSocketAddress getAddress(Process p, int n2)
	{
		InetSocketAddress sa1 = null;
		
		while(sa1 == null)
		{
			ServerSocket s = p.senders.get(n2);
			if(s != null)
			{
				sa1 = (InetSocketAddress)s.getLocalSocketAddress();
			}
		}
		return sa1;
	}
	
	
	
	public static boolean connected(int n,Process p)
	{
		return p.outputs.get(n) != null;
	}
	
	public static void connectAllServers(Process p)
	{
		System.out.println("connecting servers");
		System.out.println("number of connections: "+connected.keySet().size());
		for(int i: connected.keySet())
		{
			System.out.println("trying to connect to server: "+i);
			createConnections(connected.get(i),p);
		}
	}
	
	static public int getPID(Process p)
	{
		return p.me.index;
	}
	
	public static void breakConnection(Process p1, Process p2)
	{
		if(!connected(p2.me.index,p1))
		{
			return;
		}
		Object[] a1 = new Object[1];
		a1[0] = getPID(p1);
		Instruction cs1 = new Instruction(Instruction.CLOSESEND,a1);
		putInstruction(p2,cs1);
		
		Object[] a2 = new Object[1];
		a2[0] = getPID(p2);
		Instruction cs2 = new Instruction(Instruction.CLOSESEND,a2);
		putInstruction(p1,cs2);
				
		Object[] a3 = new Object[1];
		a3[0] = getPID(p1);
		Instruction cs3 = new Instruction(Instruction.CLOSERECEIVE,a3);
		putInstruction(p2,cs3);
		
		Object[] a4 = new Object[1];
		a4[0] = getPID(p2);
		Instruction cs4 = new Instruction(Instruction.CLOSERECEIVE,a4);
		putInstruction(p1,cs4);
		
	}
	
	public static boolean serverReached(Client c, int instrNum)
	{
		//returns true if a server has been reached for the given instruction
		return c.instr==null;
	}
	
	public static String  getURL(Client c,int n)
	{
		return "";
	}
	
	
	
	 
}
