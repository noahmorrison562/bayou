import java.util.HashMap;


public class Client extends Process{
	HashMap<String,String> playList;
	HashMap<ProcessId,Integer> writeVector;
	HashMap<ProcessId,Integer> readVector;
	
	
	
	public Client(ProcessId p) {
		super(p);
		playList = new HashMap<String,String>();
		readVector = new HashMap<ProcessId,Integer>();
		writeVector = new HashMap<ProcessId,Integer>();


		// TODO Auto-generated constructor stub
	}

	public void body()
	{
		System.out.println("in client");
		executeInstruction();
		if(isConnected())
		{
			System.out.println("shouldn't be here yet");
			recieveAll();
			processMessage();
		}
		
	}
	public void processMessage()
	{
		BayouMessage m = getMessage();
		if(m instanceof AntiEntrophyMessage)
		{
			FinishedAntiEntropyMessage fm = new FinishedAntiEntropyMessage(this.me);
			send(m.src.index,fm);
		}
		else
		{
			if(m!=null)
			{
				System.out.println("received something weird");
			}
		}
		removeMessage();
	}
	
	public void executeInstruction( )
	{
		Instruction temp = instr;
		if(temp != null)
		{
			System.out.println("executing instruction: "+temp.getInstr());
			int n = 0;
			String songName,url;
			switch(temp.getInstr())
			{

				case Instruction.PUT:
					System.out.println("start execute put instruction");
					songName = (String)instr.getArgs()[0];
					url = (String)instr.getArgs()[1];
					put(songName,url);
					break;
				case Instruction.GET:
					songName = (String)instr.getArgs()[0];
					get(songName);
					break;
				case Instruction.DELETE:
					songName = (String)instr.getArgs()[0];
					delete(songName);
					break;
				default:
					super.executeInstruction();
					break;
			}
			
			instr = null;

		}
	}
	
	
	public void put(String sn, String url)
	{
		BayouMessage put = new PutMessage(this.me,sn,url,readVector,writeVector);
		sendServer(put);
		BayouMessage m = null;
		while(m == null)
		{
			recieveAll();
			m = getMessage();
			if(!(m instanceof PutMessage))
			{
				removeMessage();
				m=null;
			}
		}
		PutMessage gm = ((PutMessage)m);
		
		if(!gm.songName.equals( "ERR_DEP") && !gm.songName.equals( "ERR_KEY") ) 
		{
			updateVector(this.writeVector,gm.writeV);
		}	
		removeMessage();
		System.out.println("finished client put");
	}
	
	public void get(String sn)
	{
		BayouMessage get = new GetMessage(this.me,sn,readVector,writeVector);
		sendServer(get);
		BayouMessage m = null;
		while(m == null)
		{
			recieveAll();
			m = getMessage();
			if(!(m instanceof GetMessage))
			{
				removeMessage();
				m=null;
			}
		}
		GetMessage gm = ((GetMessage)m);
		System.out.println(gm);
		if(!gm.URL.equals( "ERR_DEP") && !gm.URL.equals( "ERR_KEY") ) 
		{
			updateVector(this.readVector,gm.readV);
		}	
		removeMessage();
	}
	
	public void sendServer(BayouMessage m)
	{
		for(int i : outputs.keySet())
		{
			send(i,m);
		}
	}
	
	public void delete(String sn)
	{
		BayouMessage del = new DeleteMessage(this.me,sn,readVector,writeVector);
		sendServer(del);
		BayouMessage m = null;
		while(m == null)
		{
			recieveAll();
			m = getMessage();
			if(!(m instanceof DeleteMessage))
			{
				removeMessage();
				m=null;
			}
		}
		DeleteMessage gm = ((DeleteMessage)m);
		
		if(!gm.songName.equals( "ERR_DEP") && !gm.songName.equals( "ERR_KEY") ) 
		{
			updateVector(this.writeVector,gm.writeV);
		}	
		removeMessage();
	}
	
	public void updateVector(HashMap<ProcessId,Integer> v1, HashMap<ProcessId,Integer> v2)
	{
		for(ProcessId id : v2.keySet())
		{
			int n2 = v2.get(id);

			if(v1.get(id) == null)
			{
				v1.put(id,n2 );
				continue;
			}
			else if(n2 > v1.get(id) )
			{
				v1.put(id,n2);
			}		
		}
	}
	
	
}
