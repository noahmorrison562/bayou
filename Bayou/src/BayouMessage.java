import java.io.Serializable;
import java.util.HashMap;


public class BayouMessage implements Serializable {

	ProcessId src;
	public BayouMessage(ProcessId source)
	{
		this.src = source;
	}
}
	
class WriteMessage extends ClientMessage
{
	boolean committed = false;
	public WriteMessage(ProcessId source,	HashMap<ProcessId,Integer> v1,HashMap<ProcessId,Integer> v2) {
		super(source,v1,v2);
		// TODO Auto-generated constructor stub
	}
	
}

class PutMessage extends WriteMessage
{
	String songName;
	String url;
	
	public PutMessage(ProcessId source,String sn, String u,	HashMap<ProcessId,Integer> v1,HashMap<ProcessId,Integer> v2) {
		super(source,v1,v2);
		this.songName = sn;
		this.url = u;
	}
	
}


class ClientMessage extends BayouMessage
{
	HashMap<ProcessId,Integer> readV;
	HashMap<ProcessId,Integer> writeV;

	public ClientMessage(ProcessId source,	HashMap<ProcessId,Integer> v1,HashMap<ProcessId,Integer> v2) {
		super(source);
		readV = v1;
		writeV = v2;
		// TODO Auto-generated constructor stub
	}
	
}

class DeleteMessage extends WriteMessage
{
	String songName;
	public DeleteMessage(ProcessId source,String sn,	HashMap<ProcessId,Integer> v1,HashMap<ProcessId,Integer> v2) {
		super(source,v1,v2);
		this.songName = sn;
	}
	
}

class GetMessage extends ClientMessage
{
	
	String songName;
	String URL;
	public GetMessage(ProcessId source, String sn	,HashMap<ProcessId,Integer> v1,HashMap<ProcessId,Integer> v2) {
		super(source,v1,v2);
		songName = sn;
		// TODO Auto-generated constructor stub
	}
	
	public GetMessage(ProcessId source, String sn,String url,	HashMap<ProcessId,Integer> v1,HashMap<ProcessId,Integer> v2) {
		super(source,v1,v2);
		songName = sn;
		URL = url;
		// TODO Auto-generated constructor stub
	}	
	
	public String toString()
	{
		return songName+":"+URL;
	}
}

class RetireMessage extends BayouMessage
{
	boolean isPrimary;
	public RetireMessage(ProcessId source,boolean prime) {
		super(source);
		isPrimary=prime;
	}
}

class CreationMessage extends BayouMessage
{
	public CreationMessage(ProcessId source)
	{
		super(source);
	}
}

class CreationReplyMessage extends BayouMessage
{
	AcceptStamp createAcceptStamp;
	public CreationReplyMessage(ProcessId source, AcceptStamp c)
	{
		super(source);
		createAcceptStamp = c;
	}
}

class AntiEntrophyMessage extends BayouMessage
{
	HashMap<ProcessId,Integer> vv;
	int csn;
	public AntiEntrophyMessage(ProcessId source, HashMap<ProcessId,Integer> version,int c) {
		super(source);
		vv = version;
		csn = c;
		// TODO Auto-generated constructor stub
	}
	
}

class UpdateMessage extends BayouMessage
{

	Update update;
	
	public UpdateMessage(ProcessId source, Update up) {
		super(source);
		update = up;
	}
}

class CommitMessage extends UpdateMessage
{
	int csn;
	Update update;
	
	public CommitMessage(ProcessId source, int c, Update u) {
		super(source,u);
		csn = c; 
		
		// TODO Auto-generated constructor stub
	}
	
}

class TentativeMessage extends UpdateMessage
{
	AcceptStamp as;
	Update update;
	
	public TentativeMessage(ProcessId source, AcceptStamp c, Update u) {
		super(source,u);
		as = c; 
		// TODO Auto-generated constructor stub
	}
	
}

class FinishedAntiEntropyMessage extends BayouMessage
{
	public FinishedAntiEntropyMessage(ProcessId source)
	{
		super(source);
	}
}

class URPrimaryMessage extends BayouMessage
{
	public URPrimaryMessage(ProcessId src)
	{
		super(src);
	}
}
class IAmPrimaryMessage extends BayouMessage
{
	public IAmPrimaryMessage(ProcessId src)
	{
		super(src);
	}
}


