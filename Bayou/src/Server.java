import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Comparator;
import java.util.PriorityQueue;



public class Server extends Process{

	ProcessId primary;
	HashMap<String,String> tentativeState;
	HashMap<String,String> committedState;
	LinkedList<Update> tentativeWrites;
	LinkedList<Update> tentativeWritesReverse;
	LinkedList<Update> committedWrites;
	HashMap<ProcessId,Integer> vv;
	private int acceptTime;
	private int csn;
	boolean acceptFromClients;
	boolean waitingToRetire;
	boolean isPrimary;
	volatile boolean pause;
	boolean inAntiEntropy;
	int antiEntropyIndex;
	int stableCount = 0;
	volatile boolean stable = false;
	//ProcessId firstPrimary = new ProcessId(-1,"server",null,null);
	
	public Server(ProcessId m, ProcessId p,boolean prime) {
		super(m);
		primary = p;
		tentativeState = new HashMap<String,String>();
		committedState = new HashMap<String,String>();
		tentativeWrites = new LinkedList<Update>();
		tentativeWritesReverse = new LinkedList<Update>();
		vv = new HashMap<ProcessId,Integer>();
		committedWrites = new LinkedList<Update>();
		acceptTime = 0;
		csn = -1;
		acceptFromClients = false;
		waitingToRetire = false;
		isPrimary=prime;
		pause = true;
		inAntiEntropy = false;
		antiEntropyIndex = -1;
	}
	
	public void processMessage()
	{
		System.out.println(this.me.index+": in process message");
		BayouMessage m = getMessage();
		if(m instanceof WriteMessage)
		{
			if(acceptFromClients)
			{
				System.out.println("received write message and accepting from clients");
				BayouWrite((WriteMessage)m);
			}
		}
		else if(m instanceof GetMessage)
		{
			System.out.println("received get message from client");
			BayouGet((GetMessage)m);
		}
		else if(m instanceof CreationMessage)
		{
			System.out.println(this.me.getIndex()+":received creation message");
			BayouCreate((CreationMessage)m);
		}
		else if(m instanceof UpdateMessage)
		{
			System.out.println(this.me.index+":received update message");
			BayouUpdate((UpdateMessage)m);
		}
		else if(m instanceof AntiEntrophyMessage)
		{
			System.out.println(this.me.getIndex()+":received anti entropy message");
			inAntiEntropy = true;
			antiEntrophy((AntiEntrophyMessage)m);
			System.out.println(this.me.index+"received anti entropy request from "+m.src);
			inAntiEntropy = false;
		}
		else if(m instanceof URPrimaryMessage)
		{
			URPrimaryMessage um = (URPrimaryMessage)m;
			if(!(um.src.equals(primary)))
			{
				System.out.println("messed up on who we think is primary");
			}
			isPrimary = true;
			primary = this.me;
			IAmPrimaryMessage im = new IAmPrimaryMessage(this.me);
			sendAll(im);
		}
		else if(m instanceof IAmPrimaryMessage)
		{
			IAmPrimaryMessage im = (IAmPrimaryMessage)m;
			primary = im.src;
		}
		else if(m instanceof FinishedAntiEntropyMessage)
		{
			System.out.println(this.me.index+": received FinishedAntiEntropyMessage");
			inAntiEntropy = false;
			
		}
		removeMessage();
	}
	
	public void body()
	{
		//System.out.println(this.me.getIndex());
		super.body();
		if(!inAntiEntropy && !pause && outputs.keySet().size()>0)
		{
			System.out.println(this.me.index+": choosing who to antiEntropy with");
			AntiEntrophyMessage am = new AntiEntrophyMessage(this.me,vv,csn);
			//send am to someone, need to figure out how to decide who
			ObjectOutputStream o =null;
			int antiEntropyIndexCopy = antiEntropyIndex+1;
			int sendIndex=-1;
			do
			{
				antiEntropyIndex++;
				if(antiEntropyIndex>=outputs.keySet().size())
				{
					antiEntropyIndex=0;
				}
				int count = 0;
				for(int i:outputs.keySet())
				{
					sendIndex=i;
					if(count==antiEntropyIndex)
					{
						o=outputs.get(i);
						break;
					}
					count++;
				}
			}while(o==null && antiEntropyIndex==antiEntropyIndexCopy);
			if(sendIndex!=-1)
			{
				send(sendIndex,am);
				System.out.println(this.me.index+": initiated anitentropy with "+sendIndex);
				stableCount++;
				if(stableCount>outputs.keySet().size())
				{
					stable = false;
				}
			}
			
			inAntiEntropy = true;
		}
	}
	
	public void executeInstruction( )
	{
		Instruction temp = instr;
		if(temp != null)
		{
			System.out.println(this.me.getIndex()+": executing instruction "+temp.getInstr());
			int n = 0;
			switch(temp.getInstr())
			{

				case Instruction.RETIRE:
					retire();
					break;
				case Instruction.PAUSE:
					pause=true;
					break;
				case Instruction.STABLE:
					stableCount=0;
					stable=true;
					break;
				case Instruction.START:
					pause=false;
					inAntiEntropy=false;
					break;
				case Instruction.PRINT:
					for(Update w:committedWrites)
					{
						System.out.print(w);
					}
					for(Update w:tentativeWrites)
					{
						System.out.print(w);
					}
					break;
				case Instruction.CREATE:
					//System.out.println("started create");
					CreationMessage cm = new CreationMessage(this.me);
					send(primary.getIndex(),cm);
					//System.out.println("sent create message");
					BayouMessage m=null;
					while(m==null)
					{
						recieveAll();
						m = getMessage();
						if(!(m instanceof CreationReplyMessage)&&m!=null)
						{
							//System.out.println("got message not a creation reply message");
							if(isPrimary && m instanceof CreationMessage)
							{
								BayouCreate((CreationMessage)m);
								removeMessage();
								m=null;
							}
							else
							{
								removeMessage();
								m=null;
							}
						}
						else if(m!=null)
						{
							//System.out.println("received creation reply message");
							CreationReplyMessage crm = (CreationReplyMessage)m;
							if(!isPrimary)
							{
								this.me.creator = crm.src;
								this.me.creationAcceptStamp = crm.createAcceptStamp;
								acceptTime = crm.createAcceptStamp.acceptTime+1;
								vv.put(this.me, acceptTime);
								acceptFromClients = true;
								pause = false;
								
							}
							else
							{
								this.me.creator=null;
								this.me.creationAcceptStamp = crm.createAcceptStamp;
								vv.put(this.me, acceptTime);
								acceptFromClients = true;
								pause = false;
							}
							System.out.println("was created: "+this.me);
							removeMessage();
						}
					}
					break;
				default:
					super.executeInstruction();
					break;
			}
			
			instr = null;

		}
	}
	
	public void BayouWrite(WriteMessage m)
	{
		if(dependencyCheck())
		{
			update(m);
		}
		else
		{
			mergeProc();
		}
	}
	
	@Override
	public void closeRecieve(int k)
	{
		int count = 0;
		for(int i : outputs.keySet())
		{
			if(count == antiEntropyIndex)
			{
				if(i == k)
				{
					inAntiEntropy = false;
				}
				break;
			}
			count++;
		}
		
		super.closeRecieve(k);

	}
	
	public void antiEntrophy(AntiEntrophyMessage m)
	{
		System.out.println(this.me.index+": in antiEntropy method");
		HashMap<ProcessId,Integer> RV = m.vv;
		int rcsn = m.csn;
		//System.out.println("rcsn="+rcsn);
		//System.out.println("message csn="+m.csn);
		if(rcsn<csn)
		{
			for(Update w : committedWrites)
			{
				
				if(w.cStamp>rcsn)
				{
					System.out.println(this.me.index+": sent committed write");
					if(RV.get(w.aStamp.replicaId)!=null)
					{
						if(w.aStamp.acceptTime<=RV.get(w.aStamp.replicaId))
						{
							CommitMessage cm = new CommitMessage(this.me,w.cStamp,w);
							send(m.src.index,cm);
						}
						else
						{
							TentativeMessage tm = new TentativeMessage(this.me,w.aStamp,w);
							send(m.src.index,tm);
							CommitMessage cm = new CommitMessage(this.me,w.cStamp,w);
							send(m.src.index,cm);
						}
					}
					else
					{
						boolean retiredCreated = retiredOrCreated(RV,w.aStamp.replicaId);
						//check whether retired or created
						if(retiredCreated)
						{
							//receiver has seen retirement, don't need to forward writes accepted by this w.aStamp.replicaId
							//save list of replicas whose accepted writes don't need to be forwarded?
						}
						else
						{
							//send to m.src all the writes I know that this w.aStamp.replicaId has accepted
							TentativeMessage tm = new TentativeMessage(this.me,w.aStamp,w);
							send(m.src.index,tm);
							CommitMessage cm = new CommitMessage(this.me,w.cStamp,w);
							send(m.src.index,cm);
						}
						
					}
				}
			}
		}
		for(Update w:tentativeWrites)
		{
			System.out.println(this.me.index+": sent tentative write");
			System.out.println(this.me.index+": in tentative set"+w);
			if(RV.get(w.aStamp.replicaId)!=null)
			{
				if(RV.get(w.aStamp.replicaId)<w.aStamp.acceptTime)
				{
					TentativeMessage tm = new TentativeMessage(this.me,w.aStamp,w);
					send(m.src.index,tm);
				}
			}
			else
			{
				
				boolean retiredCreated = retiredOrCreated(RV,w.aStamp.replicaId);
				//check whether retired or created
				if(retiredCreated)
				{
					//receiver has seen retirement, don't need to forward writes accepted by this w.aStamp.replicaId
					//save list of replicas whose accepted writes don't need to be forwarded?
				}
				else
				{
					//System.out.println("should be here");
					//send to m.src all the writes I know that this w.aStamp.replicaId has accepted
					TentativeMessage tm = new TentativeMessage(this.me,w.aStamp,w);
					send(m.src.index,tm);
				}
			}
			
		}
		FinishedAntiEntropyMessage fm = new FinishedAntiEntropyMessage(this.me);
		send(m.src.index,fm);
		System.out.println(this.me.index+": sent finished antiEntropy to "+m.src.index);
		if(waitingToRetire)
		{
			if(isPrimary)
			{
				Master.primary = m.src;
				URPrimaryMessage um = new URPrimaryMessage(this.me);
				send(m.src.index,um);
			}
			die();
		}
		System.out.println(this.me.index+": about to exit AntiEntrophy method");
	}
	
	//TODO
	public boolean dependencyCheck()
	{
		return true;
	}
	
	//TODO
	public void mergeProc()
	{
		
	}
	
	public boolean retiredOrCreated(HashMap<ProcessId,Integer> RV,ProcessId replica)
	{
		//true = retired
		//false = created
		//check whether retired or created
		System.out.println("process sending stuff is: "+this.me.index);
		if(replica.creator==null)
		{
			//is very first primary
			if(RV.get(replica)==null)
			{
				return false;
			}
			else
			{
				if(RV.get(replica)>=replica.creationAcceptStamp.acceptTime)
				{
					//receiver has seen retirement, don't need to forward writes accepted by this w.aStamp.replicaId
					//save list of replicas whose accepted writes don't need to be forwarded?
					return true;
				}
				else
				{
					//send to m.src all the writes I know that this w.aStamp.replicaId has accepted
					return false;
				}
			}
			
		}
		else
		{
			if(RV.get(replica.creator)==null)
			{
				return retiredOrCreated(RV,replica.creator);
			}
			else
			{
				if(RV.get(replica.creator)>=replica.creationAcceptStamp.acceptTime)
				{
					//receiver has seen retirement, don't need to forward writes accepted by this w.aStamp.replicaId
					//save list of replicas whose accepted writes don't need to be forwarded?
					return true;
				}
				else
				{
					//send to m.src all the writes I know that this w.aStamp.replicaId has accepted
					return false;
				}
			}
			
		}
		
	}
	
	//TODO
	public void retire()
	{
		//write retirement update to own log?
		waitingToRetire = true;
		acceptFromClients = false;
		AcceptStamp temp = new AcceptStamp(acceptTime,this.me);
		Update retirement = new Update(null,null,null,temp,-1,false,true);
		acceptTime++;
		tentativeWrites.add(retirement);
		tentativeWritesReverse.addFirst(retirement);
		if(isPrimary)
		{
			csn++;
			Update u = new Update(retirement.songName,retirement.oldURL,retirement.newURL,retirement.aStamp,csn,retirement.creation,retirement.retirement);
			CommitMessage cm = new CommitMessage(this.me,csn,u);
			send(this.me.index,cm);
		}
		AntiEntrophyMessage am = new AntiEntrophyMessage(this.me,vv,csn);
	}
	
	public void update(WriteMessage m)
	{
		if(m instanceof PutMessage)
		{
			BayouPut((PutMessage)m);
		}
		else if( m instanceof DeleteMessage)
		{
			BayouDelete((DeleteMessage)m);
		}		

	}
	
	public void BayouPut(PutMessage m)
	{
		if(!lessThan(m.readV,this.vv) && !lessThan(m.writeV,this.vv) )
		{
			PutMessage put = new PutMessage(this.me,"END_KEY",null,null,null);
			send(m.src.index,put);
			return;
		}
		tentativeState.put(m.songName,m.url);
		AcceptStamp temp = new AcceptStamp(acceptTime,this.me);
		Update u1 = new Update(m.songName,tentativeState.get(m.songName),m.url,temp,-1,false,false);
		System.out.println("new update from client:"+temp+" "+this.me);
		tentativeWrites.add(u1);
		tentativeWritesReverse.addFirst(u1);
		vv.put(this.me, acceptTime);
		acceptTime++;
		PutMessage put = new PutMessage(this.me,"END_KEY",null,null,getRelevantWrites(m.songName));
		send(m.src.index,put);
		if(isPrimary)
		{
			csn++;
			Update u = new Update(u1.songName,u1.oldURL,u1.newURL,u1.aStamp,csn,u1.creation,u1.retirement);
			CommitMessage cm = new CommitMessage(this.me,csn,u);
			send(this.me.index,cm);
		}
		System.out.println("Server finished putting");
	}
	
	public void BayouGet(GetMessage m)
	{
		if(!tentativeState.containsKey(m.songName))
		{
			GetMessage get = new GetMessage(this.me,"ERR_KEY",null,null);
			send(m.src.index,get);
			return;
		}
		
		if(!lessThan(m.readV,this.vv) && !lessThan(m.writeV,this.vv) )
		{
			GetMessage get = new GetMessage(this.me,"ERR_DEP",null,null);
			send(m.src.index,get);
			return;
		}
		
		GetMessage get = new GetMessage(this.me,tentativeState.get(m.songName),getRelevantWrites(m.songName),null);
		send(m.src.index,get);
	}
	
	public void BayouDelete(DeleteMessage m)
	{
		if(!lessThan(m.readV,this.vv) && !lessThan(m.writeV,this.vv) )
		{
			PutMessage put = new PutMessage(this.me,"END_KEY",null,null,null);
			send(m.src.index,put);
			return;
		}
		tentativeState.remove(m.songName);
		AcceptStamp temp = new AcceptStamp(acceptTime,this.me);
		Update u1 = new Update(m.songName,tentativeState.get(m.songName),null,temp,-1,false,false);
		tentativeWrites.add(u1);
		tentativeWritesReverse.addFirst(u1);
		vv.put(this.me, acceptTime);
		acceptTime++;
		DeleteMessage del = new DeleteMessage(this.me,"END_KEY",null,getRelevantWrites(m.songName));
		send(m.src.index,del);
		if(isPrimary)
		{
			csn++;
			Update u = new Update(u1.songName,u1.oldURL,u1.newURL,u1.aStamp,csn,u1.creation,u1.retirement);
			CommitMessage cm = new CommitMessage(this.me,csn,u);
			send(this.me.index,cm);
		}
	}
	
	public void BayouCreate(CreationMessage m)
	{
		AcceptStamp temp = new AcceptStamp(acceptTime, this.me);
		Update u1;
		/*if(isPrimary)
		{
			csn++;
			u1 = new Update(null,null,null,temp,csn,true,false);
		}
		else
		{*/
			u1 = new Update(null,null,null,temp,-1,true,false);
		//}
		tentativeWrites.add(u1);
		tentativeWritesReverse.addFirst(u1);
		acceptTime++;
		CreationReplyMessage crm = new CreationReplyMessage(this.me,temp);
		send(m.src.index,crm);
		if(isPrimary)
		{
			csn++;
			Update u = new Update(u1.songName,u1.oldURL,u1.newURL,u1.aStamp,csn,u1.creation,u1.retirement);
			CommitMessage cm = new CommitMessage(this.me,csn,u);
			send(this.me.index,cm);
		}
		System.out.println(this.me.index+": send creation reply message");
	}
	public void BayouUpdate(UpdateMessage m)
	{
		if(m instanceof CommitMessage)
		{
			System.out.println(this.me.index+": received commit message");
			if((m.update.songName == null))
			{
				Update hold;
				if(!isPrimary)
				{
					this.csn = ((CommitMessage)m).csn;
					hold = new Update(m.update.songName,m.update.oldURL,m.update.newURL,m.update.aStamp,((CommitMessage)m).csn,m.update.creation,m.update.retirement);
				}
				else
				{
					hold = new Update(m.update.songName,m.update.oldURL,m.update.newURL,m.update.aStamp,-1,m.update.creation,m.update.retirement);
				}
				
				committedWrites.add(m.update);
				/*for(Update t:tentativeWrites)
				{
					System.out.println(t);
				}*/
				//System.out.println(this.me.index+" hold: "+hold);
				System.out.println(tentativeWrites.remove(hold));
				tentativeWritesReverse.remove(hold);
				executeCreationUpdates(m.update);
				return;
			}
			//System.out.println(m.update);
			boolean first = true;
			//boolean nullSN = m.update.songName==null;
			Update replace = null;
			//tentativeWrites = tentativeWrites.reverse();
			for(Update u : tentativeWritesReverse)
			{
				//System.out.println(u);
				if(u.equals(m.update))
				{
					if(first)
					{
						replace = u;
					}
				}
				else if(m.update.songName.equals(u.songName))
				{
					if(replace != null && first)
					{
						replace = u;
						break;
					}
					first = false;
				}
			}
			Update hold = new Update(m.update.songName,m.update.oldURL,m.update.newURL,m.update.aStamp,-1,m.update.creation,m.update.retirement);
			System.out.println("committed update:"+m.update);
			System.out.println(tentativeWrites.remove(hold));
			tentativeWritesReverse.remove(hold);
			System.out.println("removed: "+hold);
			System.out.println("size of tentativeWrites: "+tentativeWrites.size());
			if(replace != null)
			{
				tentativeState.put(replace.songName, replace.newURL);
			}
			updateCommitted(m.update,((CommitMessage)m).csn);
			System.out.println("finished committing");
		}
		else if(m instanceof TentativeMessage )
		{
			updateTentative(m.update);
		}


	}
		
	
	public void updateTentative(Update update)
	{
		int count = 0;
		int snIndex = -1;
		boolean nullSN = (update.songName == null);
		boolean changed = false;
		
		for(Update u : tentativeWritesReverse)
		{
			if(update.equals(u))
			{
				//update already exists
				return;
			}
			if(u.aStamp.replicaId.compareTo(update.aStamp.replicaId)==0)
			{
				if(u.aStamp.acceptTime < update.aStamp.acceptTime)
				{
					changed = true;
					addAfter(count,update);
					break;
				}
			}			
			

			count++;

		}
		
		if(!changed)
		{	
			addAfter(count,update);
		}
		if(vv.containsKey(update.aStamp.replicaId) && (vv.get(update.aStamp.replicaId) < update.aStamp.acceptTime))
		{
				vv.put(update.aStamp.replicaId, update.aStamp.acceptTime);
		}
		else if(!vv.containsKey(update.aStamp.replicaId))
		{
			vv.put(update.aStamp.replicaId, update.aStamp.acceptTime);
		}
	}
	
	public void addAfter(int num, Update update)
	{
		if(tentativeWrites.size() == 0)
		{
			addtoReverseList(tentativeWrites,tentativeWritesReverse,0,update);
			if(update.songName == null)
			{
				executeCreationUpdates(update);
			}
			else
			{
				tentativeState.put(update.songName,update.newURL);
			}
			return;
		}
		
		int n = tentativeWrites.size() - num-1;
		if(n == -1)
		{
			n = 0;
		}
		Update u = tentativeWrites.get(n);
		int count = n;
		while(count < tentativeWrites.size() && u.aStamp.replicaId.compareTo( update.aStamp.replicaId) == -1)
		{
			u = tentativeWrites.get(count);
			count++;
		}
		
		addtoReverseList(tentativeWrites,tentativeWritesReverse,count,update);
		count++;
		if(update.songName == null)
		{
			executeCreationUpdates(update);
			return;
		}
		
		while(count < tentativeWrites.size())
		{
			if(tentativeWrites.get(n).songName.equals(update.songName))
			{
				return;
			}
			count++;
		}
		
		tentativeState.put(update.songName,update.newURL);
	}
	
	
//	public void updateTentative(Update update)
//	{	
//		if(vv.containsKey(update.aStamp.replicaId) && (vv.get(update.aStamp.replicaId) < update.aStamp.acceptTime))
//		{
//			if(update.songName == null)
//			{
//				//creation and retire updates
//				executeCreationUpdates(update);
//				return;
//			}
//			tentativeState.put(update.songName, update.newURL);
//			vv.put(update.aStamp.replicaId, update.aStamp.acceptTime);
//		}
//		else
//		{
//			
//			lateUpdate(update);
//			
//		}
//	}
	
	public void executeCreationUpdates(Update update)
	{
		if(update.retirement)
		{
			vv.remove(update.aStamp.replicaId);
		}
	}

	
	
	
	public void updateCommitted(Update update, int n)
	{

		if(this.csn < n || isPrimary)
		{
			committedState.put(update.songName, update.newURL);
			if(!isPrimary)
			{
				this.csn = n;
			}
			committedWrites.add(update);
		}
	}
	
	
	public boolean lessThan(HashMap<ProcessId,Integer> v1, HashMap<ProcessId,Integer> v2)
	{
		for(ProcessId id : v2.keySet())
		{
			int n2 = v2.get(id);

			if(v1.get(id) != null && n2 < v1.get(id) )
			{
				return false;
			}
	
		}
		
		return true;
	}
	
	public HashMap<ProcessId,Integer> getRelevantWrites(String sn)
	{
		HashMap<ProcessId,Integer> relevant = new HashMap<ProcessId,Integer>();
		for(Update update : committedWrites)
		{
			if(update.songName == sn )
			{
				relevant.put(update.aStamp.replicaId,update.aStamp.acceptTime);
			}
		}
		
		for(Update update : tentativeWrites)
		{
			if(update.songName == sn )
			{
				relevant.put(update.aStamp.replicaId,update.aStamp.acceptTime);
			}
		}
		
		
		return relevant;
	}
	
	
	
//	public void lateUpdate(Update update)
//	{
//		boolean first = true;
//		int count = 0;
//		boolean nullSN = (update.songName == null);
//		boolean changed = false;
//		for(Update u : tentativeWritesReverse)
//		{
//			if(update.equals(u))
//			{
//				//update already exists
//				return;
//			}
//			if(u.aStamp.replicaId.equals(update.aStamp.replicaId))
//			{
//				if(u.aStamp.acceptTime < update.aStamp.acceptTime)
//				{
//					changed = true;
//					addtoReverseList(tentativeWritesReverse,tentativeWrites,count-1,update);
//					//tentativeWrites.add(count-1,update);
//					//tentativeWritesReverse.add(count-1,update);
//					break;
//				}
//			}			
//			if(u.songName.equals(update.songName) && !nullSN)
//			{
//
//					first = false;
//			}
//			
//
//			count++;
//
//		}
//		
//		if(first)
//		{
//			if(!nullSN)
//			{
//				tentativeState.put(update.songName,update.newURL);
//			}
//		}
//		
//		if(!changed)
//		{			
//			addtoReverseList(tentativeWritesReverse,tentativeWrites,count-1,update);
//		}
//		
//	}
	
//	public boolean isRelevantWrite(Update u)
//	{
//		for(Update update : tentativeWrites)
//		{
//			if(update.aStamp.replicaId.equals() )
//			{
//				return true;
//			}
//		}
//		
//		return false;
//	}
	
	public void addtoReverseList(LinkedList<Update> L1, LinkedList<Update> L2,int n,Update u)
	{
		if(n == L1.size())
		{
			L1.addLast(u);
			L2.addFirst(u);
			return;
		}
		L1.add(n,u);
		L2.add(L2.size()-n-1,u);
	}
	
}

//StringLengthComparator.java

//class UpdateComparator implements Comparator<Update>
//{
// @Override
// public int compare(Update x, Update y)
// {
//     // Assume neither string is null. Real code should
//     // probably be more robust
//     // You could also just return x.length() - y.length(),
//     // which would be more efficient.
//     if (x.aStamp.replicaId.equals(y.aStamp.replicaId))
//     {
//         return ((Integer)x.aStamp.acceptTime).compareTo(x.aStamp.acceptTime);
//     }
//     else
//     {
//    	return x.aStamp.replicaId.compareTo(y.aStamp.replicaId); 
//     }
// }
//
//}
//
////StringLengthComparator.java
//
//class UpdateComparator2 implements Comparator<Update>
//{
//@Override
//public int compare(Update x, Update y)
//{
//   // Assume neither string is null. Real code should
//   // probably be more robust
//   // You could also just return x.length() - y.length(),
//   // which would be more efficient.
//   int n = 0;
//	if (x.aStamp.replicaId.equals(y.aStamp.replicaId))
//   {
//        n = ((Integer)x.aStamp.acceptTime).compareTo(x.aStamp.acceptTime);
//   }
//   else
//   {
//	   n = x.aStamp.replicaId.compareTo(y.aStamp.replicaId); 
//   }
//	if(n == -1)
//	{
//		return 1;
//	}
//	if(n == 1)
//	{
//		return -1;
//	}
//	return 0;
//}
//
//}
//
